<?php get_header(); ?>

<main>
	

<section class="under_fv parallax relative"
	 data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/top_about_bg.jpg"
	 data-parallax-bg-position="center bottom"
	 data-parallax-speed="0.4"
	 data-parallax-direction="down">
	<div class="under_fv_txt absolute">
		<h2 class="h2 mainColor">お知らせ</h2>
		<p class="engTitle h1 subColor">News</p>
	</div>
</section>








<section class="pd-common relative paperBgUnder">
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
				<?php
					while ( have_posts() ) : the_post();
						get_template_part('content-post'); 
					endwhile;
				?>
			</div>
			<div class="col-sm-3">
				<?php dynamic_sidebar(); ?>
			</div>
		</div>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>
</main>
<?php get_footer(); ?>