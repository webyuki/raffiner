<section class="topFlow margin">
	<div class="container">
		<h3 class="bold h3 mb10">施工までの流れ</h3>
		<div class="titleBd mb10 titleBdLeft"></div>
		<p class="fontEn h5 bold mainColor mb30">Flow</p>
		<div class="row mb30" data-aos="fade-up">
			<div class="col-sm-2 col-xs-3" >
				<div class="bgImg bgImgCircle relative" style="background-image:url('<?php echo get_template_directory_uri();?>/img/flow_03.jpg')">
					<div class="fontNum topFlowNum absolute">1</div>
				</div>
			</div>
			<div class="col-xs-9">
				<p class="bold h4 mb10">お申込み</p>
				<p class="grayColor text_m">お住まいへの害虫被害で気になることや不安があれば、お電話・メール・問い合わせフォームからお気軽にお問い合わせください。ご相談は無料です。</p>
			</div>
		</div>
		<div class="row mb30" data-aos="fade-up">
			<div class="col-sm-2 col-xs-3">
				<div class="bgImg bgImgCircle relative" style="background-image:url('<?php echo get_template_directory_uri();?>/img/flow_04.jpg')">
					<div class="fontNum topFlowNum absolute">2</div>
				</div>
			</div>
			<div class="col-xs-9">
				<p class="bold h4 mb10">無料調査</p>
				<p class="grayColor text_m">専門スタッフがご訪問し、床下や天井裏など被害の発生している場所を無料で調査いたします。害虫・害獣の種類や被害の大きさによってかかる時間は異なりますが、1時間程度がめやすです。</p>
			</div>
		</div>
		<div class="row mb30" data-aos="fade-up">
			<div class="col-sm-2 col-xs-3">
				<div class="bgImg bgImgCircle relative" style="background-image:url('<?php echo get_template_directory_uri();?>/img/flow_01.jpg')">
					<div class="fontNum topFlowNum absolute">3</div>
				</div>
			</div>
			<div class="col-xs-9">
				<p class="bold h4 mb10">ご報告と無料見積書提出</p>
				<p class="grayColor text_m">調査の結果をご報告し、駆除や予防にかかるお見積もりを提示させていただきます。金額は被害の大きさや作業内容によって異なります。</p>
			</div>
		</div>
		<div class="row mb30" data-aos="fade-up">
			<div class="col-sm-2 col-xs-3">
				<div class="bgImg bgImgCircle relative" style="background-image:url('<?php echo get_template_directory_uri();?>/img/flow_02.jpg')">
					<div class="fontNum topFlowNum absolute">4</div>
				</div>
			</div>
			<div class="col-xs-9">
				<p class="bold h4 mb10">ご契約</p>
				<p class="grayColor text_m">見積書に同意をいただいたのちご契約を結び、作業の日程を調整いたします。</p>
			</div>
		</div>
	</div>
</section>
