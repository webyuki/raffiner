<?php get_header(); ?>
<main>
	
<section class="under_fv parallax relative"
	 data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/diet_fv.jpg"
	 data-parallax-bg-position="center bottom"
	 data-parallax-speed="0.4"
	 data-parallax-direction="down">
	<div class="under_fv_txt absolute">
		<h2 class="h3 bold">サプリなし、運動なし！<br>しっかり食べながらダイエットしませんか？</h2>
		<p class="engTitle h1 subColor">Diet counseling</p>
	</div>
</section>

<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Concept</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">ラ・フィーネ式食べ痩せダイエットとは？</h3>
			</div>
		</div>
        <div class="width720">
            <img src="<?php echo get_template_directory_uri(); ?>/img/page_diet_01.jpg" alt="ラ・フィーネ式食べ痩せダイエットとは？">
        </div>
	</div>
</section>
	
<div class="parallax parallax_diet_concept">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
                <p class="lh_xl text-center text-left-xs">食べる量を減らさず健康的に体重を減らすダイエットです。<br>高価なサプリメントや運動も必要ありません。<br>20代から変わらない体重をキープしている、<br>体の仕組みを知り尽くしたダイエットカウンセラーと一緒に<br>3か月間でなりたい体になりませんか？</p>

                
			</div>
		</div>
	</div>
</div>
	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Problem</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">こんなお悩みはありませんか</h3>
				<div class="width720">
					<ul class="diet_problem_ul">
						<li>
							<p class="diet_problem_txtarea relative h4 white bgSubColor mb10">食べていないのに体重が増えていく</p>
						</li>
						<li>
							<p class="diet_problem_txtarea relative h4 white bgSubColor mb10">食べていないのに体重が減らない</p>
						</li>
						<li>
							<p class="diet_problem_txtarea relative h4 white bgSubColor mb10">甘いものがやめられない</p>
						</li>
						<li>
							<p class="diet_problem_txtarea relative h4 white bgSubColor mb10">食欲がコントロールできない</p>
						</li>
						<li>
							<p class="diet_problem_txtarea relative h4 white bgSubColor mb10">健康診断の結果が年々悪くなっている</p>
						</li>
						<li>
							<p class="diet_problem_txtarea relative h4 white bgSubColor mb10">食べることに罪悪感を抱いてしまう</p>
						</li>
						<li>
							<p class="diet_problem_txtarea relative h4 white bgSubColor mb10">どうすれば痩せられるのかわからない</p>
						</li>
						<li>
							<p class="diet_problem_txtarea relative h4 white bgSubColor mb10">つい食べ過ぎてしまう</p>
						</li>
						<li>
							<p class="diet_problem_txtarea relative h4 white bgSubColor mb10">自己流ダイエットでいつも失敗してしまう</p>
						</li>
						<li>
							<p class="diet_problem_txtarea relative h4 white bgSubColor mb10">一人でダイエットするのがつらい</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common bgMainColor">
	<div class="container">
		<div class="row mb50">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Service</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">ラ・フィーネの食べ痩せダイエット法でできること</h3>
				<ul class="top_about_ul ul-4 ul-xs-1">
					<li>
						<!-- 実際の表示用SVG -->
						<svg class="top_about_svg mb20" width="347" height="347" viewBox="0 0 347.607 347.608">
							<image xlink:href="<?php echo get_template_directory_uri(); ?>/img/diet_about01.jpg" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" clip-path="url(#clip01)"/>
						</svg>
						<h4 class="h4 subColor text-center mb10">しっかり<br>食べて痩せる！</h4>
						<div class="mainHr subBorderColor mb20"></div>
						<p>成功のコツは、ダイエット中でもしっかり食べること。必要な栄養素で体を満たすことで、太る原因となる食べ物への欲求を抑えます。</p>
					</li>
					<li>
						<!-- 実際の表示用SVG -->
						<svg class="top_about_svg mb20" width="347" height="347" viewBox="0 0 347.607 347.608">
							<image xlink:href="<?php echo get_template_directory_uri(); ?>/img/diet_about02.jpg" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" clip-path="url(#clip01)"/>
						</svg>
						<h4 class="h4 subColor text-center mb10">健康できれいな<br>体づくり</h4>
						<div class="mainHr subBorderColor mb20"></div>
                        <!--
						<p>糖尿病・中性脂肪・高血圧・高コレステロールなど、太っていることでかかりやすい病気から体を守り、美しいボディラインを維持します。</p>
                        -->
                        <p>糖尿病、中性脂肪、高血圧、高コレステロールなど太っていることでかかりやすい病気から体を守り、体重、脂肪を落としていきます。</p>
					</li>
					<li>
						<!-- 実際の表示用SVG -->
						<svg class="top_about_svg mb20" width="347" height="347" viewBox="0 0 347.607 347.608">
							<image xlink:href="<?php echo get_template_directory_uri(); ?>/img/diet_about03.jpg" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" clip-path="url(#clip01)"/>
						</svg>
						<h4 class="h4 subColor text-center mb10">週一度<br>カウンセラーがサポート</h4>
						<div class="mainHr subBorderColor mb20"></div>
						<p>1週間に1度通って頂くことによりカウンセラーと一緒に生活を振り返って自己流ダイエットでは気づけない部分までお客様のお身体と向き合っていきます。目標達成までカウンセラーがしっかりサポートします。</p>
					</li>
					<li>
						<!-- 実際の表示用SVG -->
						<svg class="top_about_svg mb20" width="347" height="347" viewBox="0 0 347.607 347.608">
							<image xlink:href="<?php echo get_template_directory_uri(); ?>/img/diet_about04.jpg" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" clip-path="url(#clip01)"/>
						</svg>
						<h4 class="h4 subColor text-center mb10">美肌になる！<br>気持ちもポジティブに！</h4>
						<div class="mainHr subBorderColor mb20"></div>
						<p>私達の身体は食べたものでできています。栄養バランスが整うと心と身体のコンディションがよくなり内側からの美しさが引き出され美肌になります気持ちも前向きになり元気になります！</p>
					</li>
				</ul>
			</div>
		</div>
        <h3 class="mainTitle h2 mainColor text-center relative mb50">その日から実行できる！<br>食事法アドバイス付きカウンセリング</h3>
        <div class="width720">
        <p class="h3 text-center"><span class="bold">サイトを見た人限定</span> <span class="pinkColor h1">8,800</span>円 (通常10,500円) / 90分</p>

        </div>
        
	</div>
</section>
	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Flow</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">初回カウンセリングの流れ</h3>
			</div>
		</div>
        
        
        <div class="width720">
            <div class="row mb30">
                <div class="col-sm-12">
                    <h4 class="h3 subColor mb10"><span class="engTitle h1 subColor">01. </span>お問い合わせ</h4>
                    <p>ラインまたはフォームにて、ご希望の日にちと時間をお知らせ下さい。こちらから予約が可能な日程を返信します。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-12">
                    <h4 class="h3 subColor mb10"><span class="engTitle h1 subColor">02. </span>カウンセリングシートにご記入</h4>
                    <p>カウンセリングシートにご記入していただきます。食事の内容・時間・アレルギーの有無などを確認します。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-12">
                    <h4 class="h3 subColor mb10"><span class="engTitle h1 subColor">03. </span>カウンセリング</h4>
                    <p>カウンセリングシートを参考に、生活のリズムや食生活などお聞きします。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-12">
                    <h4 class="h3 subColor mb10"><span class="engTitle h1 subColor">04. </span>数値の測定</h4>
                    <p>体重、体脂肪率、内臓脂肪、基礎代謝、体内年齢を測定します。着脱しやすい服装でお越しいただけると助かります。</p>
                </div>
            </div>
            <div class="row mb30">
                <div class="col-sm-12">
                    <h4 class="h3 subColor mb10"><span class="engTitle h1 subColor">05. </span>アドバイス</h4>
                    <p>お客様のライフスタイルに合わせた、結果を出すために今日からすぐできることをアドバイスします。無理な食事制限も運動もありません。</p>
                    <p class="text_m gray">※効率よく痩せるポイントをストレスにならないようお尋ねしていきます</p>
							<p class=" h4 mainColor bold relative">2回目以降、継続される方は次回のご予約をお取り下さい</p>
                </div>
            </div>
            
            <ul class="top_course_ul relative pageDietFee mb50">
                <li>
                    <!--
                    <div class="mb30">
                        <p class="h4"><span class="bold">２回目以降</span> <span class="pinkColor h2">4,500</span>円 / 40〜45分</p>
                    </div>
                    -->
                    <div class="mb10">
                        <!--<p class="h4 lh_xl"><span class="pinkColor">◆</span>継続される方は次回のご予約をお取り下さい</p>-->
                        <p class="h4 lh_xl"><span class="pinkColor">◆</span>2回目以降は基本1週間に1度通っていただきます</p>
                        <p class="h4 lh_xl"><span class="pinkColor">◆</span>3ヶ月後、ご卒業となります</p>
                    </div>
                    <p class="text_m gray">※2回目以降は一度ご検討される方、初回だけの方、全てご自由です。</p>
                </li>
            </ul>
            
        </div>
        <h3 class="mainTitle h3 mainColor text-center relative mb50">ご来店いただかなくても、<br>オンラインを使ってのカウンセリングも可能です。</h3>
        
        
	</div>	
</section>


<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Question</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">よくあるご質問</h3>
				
				<dl class="top_qa_dl width980 flex mb20">
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q1</span>ダイエット方法が自分に合っているか確認できますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A1</span>はい、初回無料でカウンセリングを行っています。お気軽にお問い合わせください。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q2</span>どのくらいのペースでカウンセリングを受けるのですか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A2</span>基本的に、週1回のペースでカウンセリングを受けていただきます。食生活の管理や、モチベーションの維持に効果的です。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q3</span>カウンセリングだけで痩せられるのですか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A3</span>痩せられます。太っている方は食生活を見直し、必要な栄養素を補うことで体重を減らすことができます。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q4</span>運動もするのですか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A4</span>ラ・フィーネのダイエットカウンセリングでは、運動量を増やすことなく体重を減らせる方法を指導しています。過度な運動を要求することはありません。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q5</span>妊娠中でもカウンセリングを受けることはできますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A5</span>妊娠中はおすすめしていません。子育てしながらのダイエットは可能ですので、産後太りでお悩みの方はぜひご相談ください。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q6</span>高額なサプリメントの販売もありますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A6</span>サプリメントの取り扱いはしておりませんので、ご安心ください。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q7</span>自己流ですが、ダイエットをしてみたけど痩せることができませんでした。必ず痩せますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A7</span>はい、まずは初回カウンセリングにてご確認ください。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q8</span>男性でも大丈夫ですか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A8</span>申し訳ございませんが、当店は女性専用サロンとなっております。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q9</span>クレジットカードは使えますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A9</span>VISAとMastercardでのお支払いが可能です。</div></dd>
				</dl>
			</div>
		</div>
	</div>	
</section>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>