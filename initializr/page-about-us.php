<?php get_header(); ?>
<main>
	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="h1 engTitle bold mt40 mt-xs-20">ABOUT US</p>
				<h2 class="h2 bold">ジョイントリーについて</h2>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common" id="company01" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="service_area top_recruit_area pt_grad_after relative mb50">
					<div class="text-center-xs"><p class="engMainTitle engTitle white">CONCEPT</p></div>
					<div class="text-center-xs"><h3 class="h3 bold mb50 mb-xs-20">ジョイントリーは「音響」の会社です。</h3></div>
					<p class="mb20">コンサート・ライブ・イベント・コンベンションなど数多くのジャンルに対応し、最新の音響機材での短時間で的確なセッティング、細やかで正確なオペレーティングを行います。</p>
					<p class="mb20">企画が生まれた瞬間から催事に関するご相談をお受けできますので、お客様のニーズやご予算に合わせた音響プランのご提案、クオリティの高い音響サービスのご提供が可能です。</p>
					<p class="mb20">【音】をただ聞こえるようにするというだけでなく、会場にいる誰もが【良い音】を感じ、ステージの興奮や感動を、オーディエンス全体で共有できるような空間作りがJointlyの使命であると考えております。</p>
					<p class="mb10">Jointlyではこれらのコンセプトを具現化できる機材とスタッフをご用意してお待ちしております。</p>
				</div>
			</div>
		</div>
	</div>	
</section>

<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center-xs"><p class="engMainTitle engTitle white">FEATURE</p></div>
				<div class="text-center-xs"><h3 class="h3 bold mb20">ジョイントリーだからできること</h3></div>
			</div>
		</div>
	</div>
	<div class="pt_white_area_outer relative mb50" id="company_feature01">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="pt_white_area relative mb50">
						<div class="pt_white_area_inner" data-aos="fade-up">
							<div class="text-center-xs"><h4 class="engMainTitle engTitle lightblueColor mb30">01 <br class="visible-xs"><span class="h1 grayColor bold ygothic">1,000件以上の<br class="visible-xs">豊富な実績</span></h4></div>
							<p class="grayColor mb10">コンサート・ライブ・イベント・コンベンションなど数多くのジャンルに対応し、最新の音響機材での短時間で的確なセッティング、細やかで正確なオペレーティングを行います。<br>企画が生まれた瞬間から催事に関するご相談をお受けできますので、お客様のニーズやご予算に合わせた音響プランのご提案、クオリティの高い音響サービスのご提供が可能です。</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pt_white_area_outer relative mb50" id="company_feature02">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="pt_white_area relative mb50" data-aos="fade-up">
						<div class="pt_white_area_inner">
							<div class="text-center-xs"><h4 class="engMainTitle engTitle lightblueColor mb30">02 <br class="visible-xs"><span class="h1 grayColor bold ygothic">ユーザーの事を考えた<br class="visible-xs">柔軟な対応</span></h4></div>
							<p class="grayColor mb10">コンサート・ライブ・イベント・コンベンションなど数多くのジャンルに対応し、最新の音響機材での短時間で的確なセッティング、細やかで正確なオペレーティングを行います。<br>企画が生まれた瞬間から催事に関するご相談をお受けできますので、お客様のニーズやご予算に合わせた音響プランのご提案、クオリティの高い音響サービスのご提供が可能です。</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
	<div class="pt_white_area_outer relative" id="company_feature03">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="pt_white_area relative mb50" data-aos="fade-up">
						<div class="pt_white_area_inner">
							<div class="text-center-xs"><h4 class="engMainTitle engTitle lightblueColor mb30">03 <br class="visible-xs"><span class="h1 grayColor bold ygothic">最新の機器の取り揃え</span></h4></div>
							<p class="grayColor mb10">コンサート・ライブ・イベント・コンベンションなど数多くのジャンルに対応し、最新の音響機材での短時間で的確なセッティング、細やかで正確なオペレーティングを行います。<br>企画が生まれた瞬間から催事に関するご相談をお受けできますので、お客様のニーズやご予算に合わせた音響プランのご提案、クオリティの高い音響サービスのご提供が可能です。</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12" data-aos="fade-up">
				<div class="text-center"><p class="engMainTitle engTitle white">AREA</p></div>
				<h3 class="h3 bold white mb50 mb-xs-30 text-center">対応エリア</h3>
				<h4 class="h2 text-center bold mb30">岡山県下での音響なら<br class="visible-xs">お任せください。</h4>
				<p class="width720 mb100">コンサート・ライブ・イベント・コンベンションなど数多くのジャンルに対応し、最新の音響機材での短時間で的確なセッティング、細やかで正確なオペレーティングを行います。</p>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6" data-aos="fade-right">
				<div class="text-center-xs"><p class="engMainTitle engTitle white">PROFILE</p></div>
				<div class="text-center-xs"><h3 class="h3 bold mb30">代表プロフィール</h3></div>
				<img class="mb20" src="<?php echo get_template_directory_uri(); ?>/img/company_profile.jpg" alt="">
				<p class="h4 bold">ジョイントリー代表</p>
				<p class="text_m mb10">１級舞台機構調整（音響）技能士</p>
				<p class="h2 bold mb-xs-50">野山 将司</p>
			</div>
			<div class="col-sm-6" data-aos="fade-left">
				<h4 class="h2 bold mb30">顧客ニーズに対応した、<br>プロ音響トータルサービスの実施</h4>
				<table class="company_profile_table">
					<tbody>
						<tr>
							<th>1978年</th>
							<td>岡山県総社市生まれ</td>
						</tr>
						<tr>
							<th>1996年</th>
							<td>岡山県立倉敷工業高等学校卒</td>
						</tr>
						<tr>
							<th>1998年</th>
							<td>岡山科学技術専門学校　映像音響学科卒<br>専門学校の２年間に音響の基礎を学び、学生アルバイトとして現場を経験。音作りの楽しさに触れ、仕事しての喜びを見出す。</td>
						</tr>
						<tr>
							<th>1999年4月</th>
							<td>岡山県内の大手音響専門会社に入社<br>様々な現場を通じて音響技術者としての経験を重ねる。<br><br>そして数多くの【ひと】と出会い、学び、自らの研鑽に励む。</td>
						</tr>
						<tr>
							<th>2014年1月</th>
							<td>Jointlyとして起業<br>岡山県内外を問わず、フットワーク軽く活動中。</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common" id="company_description" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="top_recruit_area pt_grad_after relative">
					<div class="text-center-xs"><p class="engMainTitle engTitle white">COMPANY</p></div>
					<div class="text-center-xs"><h3 class="h3 bold mb50 mb-xs-20">会社概要</h3></div>
					<table class="company_profile_table mb-xs-30">
						<tbody>
							<tr>
								<th>会社名</th>
								<td>Jointry</td>
							</tr>
							<tr>
								<th>代表</th>
								<td>野山 将司</td>
							</tr>
							<tr>
								<th>所在地</th>
								<td>〒701-0221　岡山県岡山市丸の内3-3-3</td>
							</tr>
							<tr>
								<th>資本金</th>
								<td>1,000万円</td>
							</tr>
							<tr>
								<th>設立年月日</th>
								<td>2014年1月1日</td>
							</tr>
							<tr>
								<th>事業内容</th>
								<td>◯◯◯◯◯◯◯◯</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>	
</section>

<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	

</main>

<?php get_footer(); ?>