<?php get_header(); ?>

<main>

<!-- ファーストビュー ここから -->
<section class="relative topFvSection">
	<div class="topFv">
		<div class="main_imgBox" data-aos="fade-right">
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_fv.jpg');"></div>
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_fv_02.jpg');"></div>
			<div class="main_img" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_fv_03.jpg');"></div>
		</div>
	</div>
	<!--<div id="particles"></div>-->
	<div class="topFvBoxWrap" data-aos="fade-up">
		<div class="topFvBoxText">
			<h3 class="topFvBoxText_jp bold serif">年を重ねるほど若々しく、<br>美しくなるエステサロン</h3>
			<p class="topFvBoxText_eng subColor engTitle bold"><span>Facial Beauty Salon Raffiner</span></p>
		</div>
	</div>
</section>



<!-- ファーストビュー ここまで -->

<!--
<section class="margin">
    <div class="container">
        <a href="https://line.me/R/ti/p/%40myl2462g" target="_blank">
            <div class="f_banner_line bdBox bgWhiteColor relative">
                <img class="f_line_icon" src="<?php echo get_template_directory_uri(); ?>/img/line.png" alt="">
                <p class="h4 mb10">LINEでのやり取りも大歓迎　<span class="f_line_link text_s white tra">友達追加はこちら</span></p>
                <p class="text_m mb0 pc">お得なクーポン情報や美肌、アンチエイジングに<br>役に立つ情報を発信中です。どうぞご登録ください。</p>
            </div>
        </a>
    </div>
</section>
-->

<section id="top_news" class="pd-common bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_about_bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">News</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">お知らせ</h3>
				<table class="top_news_table width980 mb50">
					<tbody>
                    
                <?php
                    //$paged = (get_query_var('page')) ? get_query_var('page') : 1;
                    $paged = get_query_var('page');
                    $args = array(
                        'post_type' =>  'post', // 投稿タイプを指定
                        'paged' => $paged,
                        'posts_per_page' => 6, // 表示するページ数
                        'orderby'=>'date',
                        'order'=>'DESC'
                                );
                    $wp_query = new WP_Query( $args ); // クエリの指定 	
                    while ( $wp_query->have_posts() ) : $wp_query->the_post();
                        //ここに表示するタイトルやコンテンツなどを指定 
                    get_template_part('content-post-top'); 
                    endwhile;
                    //wp_reset_postdata(); //忘れずにリセットする必要がある
                    wp_reset_query();
                ?>		
                    
					</tbody>
				</table>
				<div class="text-center mb50">
					<a href="<?php echo home_url(); ?>/news" class="pt_btn">詳しく見る</a>
				</div>
			</div>
		</div>
		<div class="row">
            <div class="col-sm-6">
                <a href="https://www.instagram.com/miyuki_raffiner/" target="_blank">
                    <div class="topNewsBannerBox">
                        <div class="topNewsBannerImgWrap">
                            <img class="topNewsBannerImg" src="<?php echo get_template_directory_uri(); ?>/img/top_news_banner_ico_insta.png" alt="">
                        </div>
                        <div class="topNewsBannerTextWrap">
                            <p class="h4 mb10 mainColor">インスタグラムでも情報更新中</p>
                            <p class="topNewsBannerButton text_s white tra">インスタグラムはこちらから</p>
                        </div>
                    </div>
                </a>
            </div>
            <div class="col-sm-6">
                <a href="https://ameblo.jp/bihada-salon-raffiner" target="_blank">
                    <div class="topNewsBannerBox">
                        <div class="topNewsBannerImgWrap">
                            <img class="topNewsBannerImg blog" src="<?php echo get_template_directory_uri(); ?>/img/top_news_banner_ico_blog.jpg" alt="">
                        </div>
                        <div class="topNewsBannerTextWrap">
                            <p class="h4 mb10 mainColor">ブログもやってます</p>
                            <p class="topNewsBannerButton text_s white tra">ラフィーネのブログはこちらから</p>
                        </div>
                    </div>
                </a>
            </div>
		</div>
	</div>
</section>



<!-- About ここから -->
<!--
<section id="top_about" class="pd-common bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_about_bg.jpg');">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">About us</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60 ">明日からも、<br class="visible-xs">自分らしく輝くために</h3>
			</div>
		</div>
	</div>
</section>
-->
	
<section class="pd-common">
	<div class="container">
		<div class="row">
		    <div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">About us</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60 ">明日からも、<br class="visible-xs">自分らしく輝くために</h3>
			</div>
			<div class="col-sm-12">
				<ul class="top_about_ul ul-3 ul-xs-1">
					<li>
						<!-- 実際の表示用SVG -->
						<svg class="top_about_svg mb20" width="347" height="347" viewBox="0 0 347.607 347.608">
							<image xlink:href="<?php echo get_template_directory_uri(); ?>/img/top_about01.jpg" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" clip-path="url(#clip01)"/>
						</svg>
						<p class="engTitle h1 subColor text-center">Anti-Aging</p>
						<h4 class="h3 text-center mb10">肌を再生する、<br>クレンジングを重視した施術</h4>
						<div class="mainHr mainBorderColor mb20"></div>
						<p>肌の再生はクレンジングから。ラ・フィーネでは余分な汚れを落とし、新陳代謝をうながすことで、くすみやたるみのない肌をつくります。</p>
					</li>
					<li>
						<!-- 実際の表示用SVG -->
						<svg class="top_about_svg mb20" width="347" height="347" viewBox="0 0 347.607 347.608">
							<image xlink:href="<?php echo get_template_directory_uri(); ?>/img/top_about02.jpg" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" clip-path="url(#clip01)"/>
						</svg>
						<p class="engTitle h1 subColor text-center">Relaxation</p>
						<h4 class="h3 text-center mb10">肌と脳の深層に<br>働きかけるセラピー</h4>
						<div class="mainHr mainBorderColor mb20"></div>
						<p>ラ・フィーネでは、疲れた肌や脳をやわらかくほぐし、”幸せホルモン”と呼ばれるオキシトシンなどの分泌をうながすことで、極上のリラクゼーションを導きます。</p>
					</li>
					<li>
						<!-- 実際の表示用SVG -->
						<svg class="top_about_svg mb20" width="347" height="347" viewBox="0 0 347.607 347.608">
							<image xlink:href="<?php echo get_template_directory_uri(); ?>/img/top_about03.jpg" width="100%" height="100%" preserveAspectRatio="xMidYMid slice" clip-path="url(#clip01)"/>
						</svg>
						<p class="engTitle h1 subColor text-center">Hand Massage</p>
						<h4 class="h3 text-center mb10">肌を伝う、<br>とろける指づかい</h4>
						<div class="mainHr mainBorderColor mb20"></div>
						<p>多くのお客さまから称賛された、エステティシャン江本の”手”。とろける質感と、肌に寄りそう温感が、体内に眠る美しさを引き出します。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
<!-- About ここまで -->
	
<!-- プロフィール ここから -->
<section class="pd-common bgMainColor" id="top_profile">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Profile</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">疲れを癒し、若々しく美しい肌を<br>造るためのエステサロンを</h3>
			</div>
		</div>
		<div class="row mb30">
			<div class="col-md-5 col-sm-4">
				<img class="mb-xs-20 top_profile_img02" src="<?php echo get_template_directory_uri(); ?>/img/top_profile02.jpg" alt="日本エステティック協会認定エステティシャン 江本 美幸">
			</div>
			<div class="col-md-7 col-sm-8">
				<p class="engTitle h1 subColor">Owner therapist</p>
				<h4 class="h2 mainColor relative mb10">江本 美幸</h4>
                <p class="top_menu_cat white bgSubColor mb30">日本エステティック協会認定エステティシャン</p>
				<p class="lh_xl">ラ・フィーネのホームページへご訪問いただきありがとうございます。<br>
疲れた肌と心を癒す救世手、エステティシャン＆ダイエットカウンセラーの江本美幸です。<br>会社員として、体調の悪さをごまかしながら必死で働いていたある日。<br>ふと、「この先ずっと、この体を引きずって毎日をやり過ごすのだろうか」との思いが芽生えました。<br>人生の折り返し地点を目前に、「今のままでは、生きている意味すら見いだせない」と、夢だったエステティシャンの道を選択。<br>会社員時代に身につけていた技術をベースに、2002年の10月、39歳でラ・フィーネをオープンしました。<br>がんばる女性が心身ともにリラックスし、癒され、きれいになれるエステサロンでありたいと思っています。
</p>
			</div>
		</div>
	</div>	
</section>
<!-- プロフィール ここまで -->
	
<div id="top_menu" class="parallax"
	 data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/top_menu_bg.jpg"
	 data-parallax-bg-position="center bottom"
	 data-parallax-speed="0.4"
	 data-parallax-direction="down"
>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 white text-center">Menu</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">施術メニュー</h3>
			</div>
		</div>
	</div>
</div>
	
<!-- メニュー ここから -->
<section class="pd-common">
	<div class="container">
		<div class="row mb50">
			<div class="col-sm-8 mb-sm-30">
				<div class="top_menu_left relative">
					<h5 class="h4 subColor">毛穴の大そうじ</h5>
					<h4 class="h2 mainColor mb10">毛穴洗浄エクストラクション</h4>
					<p class="top_menu_cat white bgSubColor mb20 mb-xs-10">毛穴ケア</p>
					<p class="h3 mainColor mb10"><span class="h0 subColor">10,500</span>円（税抜）/ <span class="h3">80</span>分</p>
					<p class="top_menu_first_price text_m white mb30">初めての方割引料金　<br class="visible-sm visible-xs"><span class="h1">8,800</span>円（税抜）</p>
					<div class="pt_border mb30"></div>
					<p class="top_menu_txt mb30">日本では珍しい毛穴洗浄技術エクストラクションは毛穴の奥から皮脂や黒ずみ、汚れを一気に取り除いていちご鼻を解消します。 <br><span class="bold mainColor">おすすめオプション：ヘッドマッサージ、デコルテマッサージ、ハンドマッサージ</span></p>
					<ul class="top_menu_tag">
						<li class="mainBorderColor text_s mb10">いちご鼻</li>
						<li class="mainBorderColor text_s mb10">毛穴の黒ずみ</li>
						<li class="mainBorderColor text_s mb10">皮脂詰まり</li>
						<li class="mainBorderColor text_s mb10">皮脂臭</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="width620 top_menu_imgarea">
					<ul class="top_menu_ul ul-3">
						<li>
							<div class="top_menu_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_menu01_bf.jpg');"></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		<!--
		<div class="row mb50">
			<div class="col-sm-8 mb-sm-30">
				<div class="top_menu_left relative">
					<h5 class="h4 subColor">肌の大そうじ</h5>
					<h4 class="h2 mainColor mb10">肌再生スペシャル・クレンジング</h4>
					<p class="top_menu_cat white bgSubColor mb20 mb-xs-10">フェイシャルエステ</p>
					<p class="h3 mainColor mb10"><span class="h0 subColor">3,000</span>円（税抜）/ <span class="h3">30</span>分</p>
					<div class="pt_border mb30"></div>
					<p class="top_menu_txt mb30">お客さまに「この手が欲しい！！」と言わせる、エステティシャン江本の手技を堪能できるスペシャル・クレンジング。ノンオイルのアミノ酸クレンジングと、まるでマッサージされているような気持ちのいいクレンジングで古い角質を取り除き、くすみのないクリアな肌に導きます。 <br><span class="bold mainColor">おすすめオプション：マッサージ・肌別パック
<br>※足湯・眉カット付き</span></p>
					<ul class="top_menu_tag">
						<li class="mainBorderColor text_s mb10">くすみ</li>
						<li class="mainBorderColor text_s mb10">クレンジング不足</li>
						<li class="mainBorderColor text_s mb10">エステはじめて</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="width620 top_menu_imgarea">
					<ul class="top_menu_ul ul-3">
						<li>
							<div class="top_menu_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_menu01.jpg');"></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		-->
		<div class="row mb50">
			<div class="col-sm-8 mb-sm-30">
				<div class="top_menu_left relative">
					<h5 class="h4 subColor">ベーシックなフェイシャルコース</h5>
					<h4 class="h2 mainColor mb10">角質ケアコース</h4>
					<p class="top_menu_cat white bgSubColor mb20 mb-xs-10">フェイシャルケア</p>
					<p class="h3 mainColor mb10"><span class="h0 subColor">8,200</span>円（税抜）/ <span class="h3">60</span>分</p>
					<p class="top_menu_first_price text_m white mb30">初めての方割引料金　<br class="visible-sm visible-xs"><span class="h1">6,200</span>円（税抜）</p>
					<div class="pt_border mb30"></div>
					<p class="top_menu_txt mb30">古い角質を取りのぞき、乾燥・くすみ・毛穴の黒ずみ・テカりが気にならないツヤ肌に整えます。<br>
                    <span class="bold mainColor">おすすめオプション：デコルテマッサージ・ヘッドマッサージ・首ほぐし</span></p>
					<ul class="top_menu_tag">
						<li class="mainBorderColor text_s mb10">くすみ</li>
						<li class="mainBorderColor text_s mb10">毛穴の黒ずみ</li>
						<li class="mainBorderColor text_s mb10">テカり</li>
						<li class="mainBorderColor text_s mb10">乾燥</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="width620 top_menu_imgarea">
					<ul class="top_menu_ul ul-3">
						<li>
							<div class="top_menu_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_menu02.jpg');"></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		
		<div class="row mb50">
			<div class="col-sm-8 mb-sm-30">
				<div class="top_menu_left relative">
					<h5 class="h4 subColor">とろけるマッサージと炭酸で肌に活力</h5>
					<h4 class="h2 mainColor mb10">アンチエイジング・炭酸パックコース</h4>
					<p class="top_menu_cat white bgSubColor mb20 mb-xs-10">アンチエイジング</p>
					<p class="h3 mainColor mb10"><span class="h0 subColor">10,500</span>円（税抜）/ <span class="h3">90</span>分</p>
					<p class="top_menu_first_price text_m white mb30">初めての方割引料金　<br class="visible-sm visible-xs"><span class="h1">8,800</span>円（税抜）</p>
					<div class="pt_border mb30"></div>
					<p class="top_menu_txt mb30">肌の衰えを感じる方に。とろけるほど気持ちのいいマッサージと炭酸パックでリフトアップされた若々しい肌に導きます。西洋医学と東洋医学にもとづいた独自のマッサージ法により、機械は一切使わず、ドライハンドで肌の深部へのマッサージを行います。日本の指圧と中国の導引によって、身体の持つ自然な回復力を再生します。更年期の不調でお悩みの方もぜひお試しください。<br><span class="bold mainColor">おすすめオプション：ヘッドマッサージ・首ほぐし・目のマッサージ</span></p>
					<ul class="top_menu_tag">
						<li class="mainBorderColor text_s mb10">更年期の不調</li>
						<li class="mainBorderColor text_s mb10">自律神経の乱れ</li>
						<li class="mainBorderColor text_s mb10">たるみ</li>
						<li class="mainBorderColor text_s mb10">シワ</li>
						<li class="mainBorderColor text_s mb10">シミ</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="width620 top_menu_imgarea">
					<ul class="top_menu_ul ul-3">
						<li>
							<div class="top_menu_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_menu03.jpg');"></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="row mb50">
			<div class="col-sm-8 mb-sm-30">
				<div class="top_menu_left relative">
					<h5 class="h4 subColor">キュッ！とした小顔を造る</h5>
					<h4 class="h2 mainColor mb10">アンチエイジング・小顔コース</h4>
					<p class="top_menu_cat white bgSubColor mb20 mb-xs-10">アンチエイジング</p>
					<p class="h3 mainColor mb10"><span class="h0 subColor">10,500</span>円（税抜）/ <span class="h3">90</span>分</p>
					<p class="top_menu_first_price text_m white mb30">初めての方割引料金　<br class="visible-sm visible-xs"><span class="h1">8,800</span>円（税抜）</p>
					<div class="pt_border mb30"></div>
					<p class="top_menu_txt mb30">むくみ・たるみ・ほうれい線・目のたるみが気になる方に。顔や首のこりをほぐしながらフェイスラインを整えます。施術後は小顔効果で目元もスッキリ。まるで整体とエステがひとつになったようなイタ気持ちいいマッサージに、やみつきになるお客さまが続出しています。<br><span class="bold mainColor">おすすめオプション： ヘッドマッサージ・ハンドマッサージ・背中マッサージ</span></p>
					<ul class="top_menu_tag">
						<li class="mainBorderColor text_s mb10">更年期の不調</li>
						<li class="mainBorderColor text_s mb10">自律神経の乱れ</li>
						<li class="mainBorderColor text_s mb10">肩、首こり</li>
						<li class="mainBorderColor text_s mb10">たるみ</li>
						<li class="mainBorderColor text_s mb10">むくみ</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="width620 top_menu_imgarea">
					<ul class="top_menu_ul ul-3">
						<li>
							<div class="top_menu_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_menu04.jpg');"></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
        
		<div class="row mb50">
			<div class="col-sm-8 mb-sm-30">
				<div class="top_menu_left relative">
					<h5 class="h4 subColor">体中心のマッサージで美肌を作る</h5>
					<h4 class="h2 mainColor mb10">敏感肌フェイシャルコース</h4>
					<p class="top_menu_cat white bgSubColor mb20 mb-xs-10">フェイシャルケア</p>
					<p class="h3 mainColor mb10"><span class="h0 subColor">9,000</span>円（税抜）/ <span class="h3">60</span>分</p>
					<p class="top_menu_first_price text_m white mb30">初めての方割引料金　<br class="visible-sm visible-xs"><span class="h1">7,000</span>円（税抜）</p>
					<div class="pt_border mb30"></div>
					<p class="top_menu_txt mb30">背中とデコルテ、シンプルフェイシャルケアを組み合わせた、敏感肌の方のためのコースです。体からのアプローチを中心とし、顔への強い刺激がある施術は行いません。アレルギーのある方にも安心してお選びいただけます。</p>
					<ul class="top_menu_tag">
						<li class="mainBorderColor text_s mb10">敏感肌</li>
						<li class="mainBorderColor text_s mb10">アレルギー肌</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="width620 top_menu_imgarea">
					<ul class="top_menu_ul ul-3">
						<li>
							<div class="top_menu_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_menu05.jpg');"></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
        
		<div class="row mb50">
			<div class="col-sm-8 mb-sm-30">
				<div class="top_menu_left relative">
					<h5 class="h4 subColor">透明感のあるツヤ肌へ</h5>
					<h4 class="h2 mainColor mb10">ホワイトニングコース</h4>
					<p class="top_menu_cat white bgSubColor mb20 mb-xs-10">アンチエイジング</p>
					<p class="h3 mainColor mb10"><span class="h0 subColor">10,500</span>円（税抜）/ <span class="h3">90</span>分</p>
					<p class="top_menu_first_price text_m white mb30">初めての方割引料金　<br class="visible-sm visible-xs"><span class="h1">8,800</span>円（税抜）</p>
					<div class="pt_border mb30"></div>
					<p class="top_menu_txt mb30">ビタミンCの美容液が、日焼けした肌やシミ・くすみに働きかけ、透明感のあるツヤ肌に整えます。<br><span class="bold mainColor">おすすめオプション： デコルテクレンジング・デコルテマッサージ</span></p>
					<ul class="top_menu_tag">
						<li class="mainBorderColor text_s mb10">日焼け肌</li>
						<li class="mainBorderColor text_s mb10">シミ</li>
						<li class="mainBorderColor text_s mb10">くすみ</li>
						<li class="mainBorderColor text_s mb10">毛穴</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="width620 top_menu_imgarea">
					<ul class="top_menu_ul ul-3">
						<li>
							<div class="top_menu_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_menu06.jpg');"></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
		
		<div class="row mb50">
			<div class="col-sm-8 mb-sm-30">
				<div class="top_menu_left relative">
					<h5 class="h4 subColor">疲れを取り、脳を癒すマッサージコース</h5>
					<h4 class="h2 mainColor mb10">リフトアップ・ヘッドマッサージ</h4>
					<p class="top_menu_cat white bgSubColor mb20 mb-xs-10">ヘッドマッサージ</p>
					<p class="h3 mainColor mb10"><span class="h0 subColor">3,000</span>円（税抜）/ <span class="h3">30</span>分</p>
					<p class="top_menu_first_price text_m white mb30">フェイシャルケアとセットで　<br class="visible-sm visible-xs"><span class="h1">2,500</span>円（税抜）</p>
					<div class="pt_border mb30"></div>
					<p class="top_menu_txt mb30">ヘッド・首・耳・デコルテ・指先までのマッサージです。立体感のある仕上がりは、たるみが気になる方にもおすすめ。肩こり、首こり、頭痛を和らげ、フェイスラインもシャープな仕上がりです。少し強めのタッチが好きな方にもご案内しています。フェイシャルケアと組み合わせることで、より高い効果が期待できます。</p>
					<ul class="top_menu_tag">
						<li class="mainBorderColor text_s mb10">頭痛</li>
						<li class="mainBorderColor text_s mb10">ほうれい線、額のしわ</li>
						<li class="mainBorderColor text_s mb10">首肩のこり</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="width620 top_menu_imgarea">
					<ul class="top_menu_ul ul-3">
						<li>
							<div class="top_menu_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_menu07.jpg');"></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
        
        
		
		<div class="row mb0">
			<div class="col-sm-8 mb-sm-30">
				<div class="top_menu_left relative">
					<h5 class="h4 subColor">デトックスとストレスケアのコース</h5>
					<h4 class="h2 mainColor mb10">リンパドレナージュ・ヘッドマッサージ</h4>
					<p class="top_menu_cat white bgSubColor mb20 mb-xs-10">ヘッドマッサージ</p>
					<p class="h3 mainColor mb10"><span class="h0 subColor">3,000</span>円（税抜）/ <span class="h3">30</span>分</p>
					<p class="top_menu_first_price text_m white mb30">フェイシャルケアとセットで　<br class="visible-sm visible-xs"><span class="h1">2,500</span>円（税抜）</p>
					<div class="pt_border mb30"></div>
					<p class="top_menu_txt mb30">ヘッド・首・耳・デコルテ・指先までのマッサージです。顔色が悪い、くすみやすい、シミ、むくみでお悩みの方におすすめ。体に溜まった老廃物の排出をうながし、自律神経に働きかけます。フェイシャルケアと組み合わせることで、より高い効果が期待できます。</p>
					<ul class="top_menu_tag">
						<li class="mainBorderColor text_s mb10">顔色悪い</li>
						<li class="mainBorderColor text_s mb10">くすみやすい</li>
						<li class="mainBorderColor text_s mb10">むくみ</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="width620 top_menu_imgarea">
					<ul class="top_menu_ul ul-3">
						<li>
							<div class="top_menu_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_menu08.jpg');"></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
        
		<div class="row mb50">
			<div class="col-sm-8 mb-sm-30">
				<div class="top_menu_left relative">
					<!--<h5 class="h4 subColor">疲れを取り、脳を癒すマッサージコース</h5>-->
					<h4 class="h2 mainColor mb10">ボディ・オイルマッサージ</h4>
					<p class="top_menu_cat white bgSubColor mb20 mb-xs-10">リラクゼーション</p>
					<p class="h4 mainColor mb0"><span class="h1 subColor">4,000</span>円（税抜）/ <span class="h3">30</span>分 <span class="top_menu_first_price text_m white">初めての方割引 <span class="h2">3,200</span>円（税抜）</span>
					<p class="h4 mainColor mb0"><span class="h1 subColor">8,000</span>円（税抜）/ <span class="h3">60</span>分 <span class="top_menu_first_price text_m white">初めての方割引 <span class="h2">6,000</span>円（税抜）</span></p>
					<p class="h4 mainColor mb10"><span class="h1 subColor">12,000</span>円（税抜）/ <span class="h3">90</span>分 <span class="top_menu_first_price text_m white">初めての方割引 <span class="h2">9,000</span>円（税抜）</span></p>
					<div class="pt_border mb30"></div>
					<p class="top_menu_txt mb30">波のリズムのマッサージで、ゆっくりと深いリラクゼーションへ導きます。エステティシャンのあたたかな手は、味わったことのない気持ち良さ。力が抜けてゆるむ感覚、深い呼吸で生き返ってください。</p>
					<ul class="top_menu_tag">
						<li class="mainBorderColor text_s mb10">全身の疲れ</li>
						<li class="mainBorderColor text_s mb10">冷え</li>
					</ul>
				</div>
			</div>
			<div class="col-sm-4">
				<div class="width620 top_menu_imgarea">
					<ul class="top_menu_ul ul-3">
						<li>
							<div class="top_menu_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_menu09.jpg');"></div>
						</li>
					</ul>
				</div>
			</div>
		</div>
        <div class="width720">
            <h3 class="mainTitle h2 mainColor text-center relative mb50">全てのフェイシャルメニューに下記サービス付き！</h3>
            <div class="width720 text-center">
                    <p class="h4 lh_xl"><span class="pinkColor">◆</span>足湯サービス</p>
                    <p class="h4 lh_xl"><span class="pinkColor">◆</span>眉カットサービス付き</p>
            </div>
        </div>
        
	</div>	
</section>
<!-- メニュー ここまで -->
	

<!-- オプションメニュー ここから -->
<section class="pd-common" id="top_option">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Option</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">オプションメニュー</h3>
			</div>
			<div class="col-sm-12">
				<ul class="top_option_ul ul-2 ul-xs-1">
                
					<li>
                        <div class="row">
                            <div class="col-sm-3">
                                <img class="topMenuOpImg" src="<?php echo get_template_directory_uri(); ?>/img/top_menu_op_01.jpg" alt="眼精疲労マッサージ">
                            </div>
                            <div class="col-sm-9">
                                <h4 class="h3 mainColor mb0">眼精疲労マッサージ</h4>
                                <p class="h4 mainColor mb10"><span class="h1 subColor">1,200</span>円（税抜）/ <span class="h3">10</span>分</p>
                                <div class="pt_border mb30"></div>
                            </div>
                        </div>
						<p class="top_menu_txt lh_xl mb10">目の周りに現れやすい疲労や老化。眼精疲労・重いまぶた・下垂・むくみ・クマ・乾燥・シワ・くぼみ・目元を若返らせ、目力のある若々しい目元にします。</p>
					</li>
					<li>
                        <div class="row">
                            <div class="col-sm-3">
                                <img class="topMenuOpImg" src="<?php echo get_template_directory_uri(); ?>/img/top_menu_op_02.jpg" alt="アイ・トリートメントマスク">
                            </div>
                            <div class="col-sm-9">
                                <h4 class="h3 mainColor mb0">アイ・トリートメントマスク</h4>
                                <p class="h4 mainColor mb10"><span class="h1 subColor">1,800</span>円（税抜）/ <span class="h3">15</span>分</p>
                                <div class="pt_border mb30"></div>
                            </div>
                        </div>
						<p class="top_menu_txt lh_xl mb10">冷たいパックで疲労回復。目元のトラブルを解消してハリのあるすっきりとした若々しい目元にします。<br><span class="bold mainColor">※アイメイキングマッサージと一緒にすると効果的、マッサージと一緒で2,500円</span></p>
					</li>
					<li>
                        <div class="row">
                            <div class="col-sm-3">
                                <img class="topMenuOpImg" src="<?php echo get_template_directory_uri(); ?>/img/top_menu_op_03.jpg" alt="首ほぐしマッサージ">
                            </div>
                            <div class="col-sm-9">
                                <h4 class="h3 mainColor mb0">首ほぐしマッサージ</h4>
                                <p class="h4 mainColor mb10"><span class="h1 subColor">1,200</span>円（税抜）/ <span class="h3">10</span>分</p>
                                <div class="pt_border mb30"></div>
                            </div>
                        </div>
						<p class="top_menu_txt lh_xl mb10">ストレス、パソコン、眼精疲労、頭痛、肩こりの方、お試しください。首のコリをほぐします。</p>
					</li>
					<li>
                        <div class="row">
                            <div class="col-sm-3">
                                <img class="topMenuOpImg" src="<?php echo get_template_directory_uri(); ?>/img/top_menu_op_04.jpg" alt="ヘッド・マッサージ">
                            </div>
                            <div class="col-sm-9">
                                <h4 class="h3 mainColor mb0">ヘッド・マッサージ</h4>
                                <p class="h4 mainColor mb10"><span class="h1 subColor">1,200</span>円（税抜）/ <span class="h3">10</span>分</p>
                                <div class="pt_border mb30"></div>
                            </div>
                        </div>
						<p class="top_menu_txt lh_xl mb10">目の疲れ、肩こり、頭皮のマッサージでリフトアップ。終わった後はすっきり！目元もパッチリ！</p>
					</li>
					<li>
                        <div class="row">
                            <div class="col-sm-3">
                                <img class="topMenuOpImg" src="<?php echo get_template_directory_uri(); ?>/img/top_menu_op_05.jpg" alt="ヘッド・マッサージ">
                            </div>
                            <div class="col-sm-9">
                                <h4 class="h3 mainColor mb0">ヘッド・マッサージ</h4>
                                <p class="h4 mainColor mb10"><span class="h1 subColor">1,200</span>円（税抜）/ <span class="h3">10</span>分</p>
                                <div class="pt_border mb30"></div>
                            </div>
                        </div>
						<p class="top_menu_txt lh_xl mb10">顔まわりのリンパを流すと顔がとてもきれいになります。終わった後の顔色はピンク色。</p>
					</li>
					<li>
                        <div class="row">
                            <div class="col-sm-3">
                                <img class="topMenuOpImg" src="<?php echo get_template_directory_uri(); ?>/img/top_menu_op_08.jpg" alt="肩〜二の腕〜手〜指先マッサージ">
                            </div>
                            <div class="col-sm-9">
                                <h4 class="h3 mainColor mb0">肩〜二の腕〜手〜指先マッサージ</h4>
                                <p class="h4 mainColor mb10"><span class="h1 subColor">1,200</span>円（税抜）/ <span class="h3">10</span>分</p>
                                <div class="pt_border mb30"></div>
                            </div>
                        </div>
						<p class="top_menu_txt lh_xl mb10">生活の中で一番よく使っている手をやさしくマッサージして疲れをとります。</p>
					</li>
					<li>
                        <div class="row">
                            <div class="col-sm-3">
                                <img class="topMenuOpImg" src="<?php echo get_template_directory_uri(); ?>/img/top_menu_op_06.jpg" alt="肩・背・肩甲骨マッサージ">
                            </div>
                            <div class="col-sm-9">
                                <h4 class="h3 mainColor mb0">肩・背・肩甲骨マッサージ</h4>
                                <p class="h4 mainColor mb10"><span class="h1 subColor">1,500</span>円（税抜）/ <span class="h3">15</span>分</p>
                                <div class="pt_border mb30"></div>
                            </div>
                        </div>
						<p class="top_menu_txt lh_xl mb10">かたくなった背中・首をほぐしてこりをやわらげます。</p>
					</li>
					<li>
                        <div class="row">
                            <div class="col-sm-3">
                                <img class="topMenuOpImg" src="<?php echo get_template_directory_uri(); ?>/img/top_menu_op_07.jpg" alt="太もも・ふくらはぎ・足裏マッサージ">
                            </div>
                            <div class="col-sm-9">
                                <h4 class="h3 mainColor mb0">太もも・ふくらはぎ・足裏マッサージ</h4>
                                <p class="h4 mainColor mb10"><span class="h1 subColor">1,500</span>円（税抜）/ <span class="h3">15</span>分</p>
                                <div class="pt_border mb30"></div>
                            </div>
                        </div>
						<p class="top_menu_txt lh_xl mb10">表面・裏面どちらかお選びいただけます。太もも〜ふくらはぎ〜足裏。イタキモマッサージでリンパを促します。むきみ・足ダル解消。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>	
</section>
<!-- オプションメニュー ここまで -->
	
	
<?php
//パラメータ（GET）の値を取得して$paramに代入
$param = $_GET["param"];
//$paramがtest01だった表示"
if ( $param == "demo" ): ?>


<!-- ビフォー・アフター ここから -->
<section class="pd-common" id="top_option">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Before &amp; After</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">ビフォー・アフター</h3>
			</div>
		</div>
		<div class="row">
            <div class="col-sm-4 col-xs-6 colBf matchHeight">
                <div class="topBfBox">
                    <div class="topBfFlex">
                        <div class="topBfCol before">
                            <div class="topBfImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_bf_01_b.jpg')">
                                <div class="topBfCaption engTitle">Before</div>
                            </div>
                        </div>
                        <div class="topBfCol after">
                            <div class="topBfImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_bf_01_a.jpg')">
                                <div class="topBfCaption engTitle">After</div>
                            </div>
                        </div>
                    </div>
                    <h4 class="topBfTitle mainColor h4 mb10">40代 毛穴洗浄エクストラクション</h4>
                    <p class="topBfText">何年も毛穴でお悩みでした。</p>
                    
                </div>
            </div>
            <div class="col-sm-4 col-xs-6 colBf matchHeight">
                <div class="topBfBox">
                    <div class="topBfFlex">
                        <div class="topBfCol before">
                            <div class="topBfImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_bf_02_b.jpg')">
                                <div class="topBfCaption engTitle">Before</div>
                            </div>
                        </div>
                        <div class="topBfCol after">
                            <div class="topBfImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_bf_02_a.jpg')">
                                <div class="topBfCaption engTitle">After</div>
                            </div>
                        </div>
                    </div>
                    <h4 class="topBfTitle mainColor h4 mb10">30代 毛穴洗浄エクストラクション</h4>
                    
                </div>
            </div>
            <div class="col-sm-4 col-xs-6 colBf matchHeight">
                <div class="topBfBox">
                    <div class="topBfFlex">
                        <div class="topBfCol before">
                            <div class="topBfImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_bf_03_b.jpg')">
                                <div class="topBfCaption engTitle">Before</div>
                            </div>
                        </div>
                        <div class="topBfCol after">
                            <div class="topBfImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_bf_03_a.jpg')">
                                <div class="topBfCaption engTitle">After</div>
                            </div>
                        </div>
                    </div>
                    <h4 class="topBfTitle mainColor h4 mb10">20代 毛穴洗浄エクストラクション</h4>
                    
                </div>
            </div>
            <div class="col-sm-4 col-xs-6 colBf matchHeight">
                <div class="topBfBox">
                    <div class="topBfFlex">
                        <div class="topBfCol before">
                            <div class="topBfImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_bf_04_b.jpg')">
                                <div class="topBfCaption engTitle">Before</div>
                            </div>
                        </div>
                        <div class="topBfCol after">
                            <div class="topBfImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_bf_04_a.jpg')">
                                <div class="topBfCaption engTitle">After</div>
                            </div>
                        </div>
                    </div>
                    <h4 class="topBfTitle mainColor h4 mb10">50代 毛穴洗浄エクストラクション</h4>
                    <p class="topBfText">あご先の黒ずみを対応いたしました。</p>                    
                </div>
            </div>
            <div class="col-sm-4 col-xs-6 colBf matchHeight">
                <div class="topBfBox">
                    <div class="topBfFlex">
                        <div class="topBfCol before">
                            <div class="topBfImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_bf_05_b.jpg')">
                                <div class="topBfCaption engTitle">Before</div>
                            </div>
                        </div>
                        <div class="topBfCol after">
                            <div class="topBfImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_bf_05_a.jpg')">
                                <div class="topBfCaption engTitle">After</div>
                            </div>
                        </div>
                    </div>
                    <h4 class="topBfTitle mainColor h4 mb10">20代 毛穴洗浄エクストラクション</h4>
                </div>
            </div>
            <div class="col-sm-4 col-xs-6 colBf matchHeight">
                <div class="topBfBox">
                    <div class="topBfFlex">
                        <div class="topBfCol before">
                            <div class="topBfImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_bf_06_b.jpg')">
                                <div class="topBfCaption engTitle">Before</div>
                            </div>
                        </div>
                        <div class="topBfCol after">
                            <div class="topBfImg bgImg" style="background-image:url('<?php echo get_template_directory_uri();?>/img/top_bf_06_a.jpg')">
                                <div class="topBfCaption engTitle">After</div>
                            </div>
                        </div>
                    </div>
                    <h4 class="topBfTitle mainColor h4 mb10">20代 毛穴洗浄エクストラクション</h4>
                </div>
            </div>
            
            
		</div>
	</div>	
</section>
<!-- ビフォー・アフター ここまで -->
	
<?php else:?>


<?php endif;?>



<section class="pd-common f_contact relative">
	<div class="">
		<div class="container">
            <div class="text-center">
                <p class="h2 yellowColor mb10">お気軽にご相談ください</p>
            </div>
            <div class="width720 mb30">
                <p class="white lh_xl mb-xs-30 text-center"><!--これはほんの一部のメニューです。本来はお客様のご要望に応じて、最適なコースをご提案いたしますので、-->少しでも興味がございましたらお気軽にお問い合わせください。<br>相談だけでも大歓迎です。</p>
            </div>
            <div class="text-center">
                <div class="footerContactTelCont mb30">
                    <a href="<?php echo home_url(); ?>/contact" class="pt_btn pt_btn_contact h4 mb10"><svg class="icon icon_mail" viewBox="0 0 31.396 24.207"><use xlink:href="#icon_mail"/></svg>お問い合わせはこちら</a>
                    <a href="tel:08019061867" class="pt_btn pt_btn_contact h2"><svg class="icon icon_mail" viewBox="0 0 31.428 23.387"><use xlink:href="#icon_tel"/></svg>080-1906-1867</a>
                </div>
                <div class="text-center mb30">
                    <p class="text_m white">営業時間[10:00〜18:30] 定休日[金]<br>時間外のご予約も承りますので、お気軽にご相談ください。</p>
                </div>
                <a href="https://line.me/R/ti/p/%40myl2462g" target="_blank">
                    <div class="f_banner_line bdBox bgWhiteColor relative">
                        <img class="f_line_icon" src="<?php echo get_template_directory_uri(); ?>/img/line.png" alt="">
                        <p class="h4 mb10">LINEでのやり取りも大歓迎　<span class="f_line_link text_s white tra">友達追加はこちら</span></p>
                        <p class="text_m mb0 pc">お得なクーポン情報や美肌、アンチエイジングに<br>役に立つ情報を発信中です。どうぞご登録ください。</p>
                    </div>
                </a>
			</div>
		</div>
	</div>
</section>


<!-- 化粧品 ここから -->
<section class="pd-common pb0">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Lotion</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">当店が使用している化粧品</h3>
                <div class="width720 mb30">
                    <img src="<?php echo get_template_directory_uri(); ?>/img/top_lotion_image.jpg" alt="当店が使用している化粧品">
                </div>
				<div class="top_lotion_txtarea width620 subBorderColor text-center mb50">
					<h4 class="h3 mainColor mb10">セレス・アルファシリーズの特徴</h4>
					<p class="lh_xl mb0 text-left-xs"><span class="bold">ノンオイル・ノン界面活性剤・ノンアルコール</span>で、<br>肌に優しく効果を発揮します。<br>洗顔・化粧水・美容液で簡単ステップ。<br>肌に必要な角質ケアでターンオーバーをうながし、<br>透明感のあるツヤ肌を実現します。<br>ホームケアとしてもおすすめしています。</p>
				</div>
				<ul class="top_lotion_ul ul-4 ul-sm-2 ul-xs-1">
					<li>
						<div class="top_lotion_img bg-common mainBorderColor mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_lotion01.jpg');"></div>
						<h4 class="h4 mainColor mb10">リファイニングジェル(洗顔料)</h4>
						<p class="mainColor text-right">300ml 5,000円（税別）</p>
						<p class="lh_xl">肌にやさしいアミノ酸パワーで、毛穴や汗口、角質層の中に入りこんだ汚れやメイク、過剰な皮脂をやさしく溶かし出して落とします。<br>まつげのエクステをしている方にもご利用いただけます。</p>
					</li>
					<li>
						<div class="top_lotion_img bg-common mainBorderColor mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_lotion02.jpg');"></div>
						<h4 class="h4 mainColor mb10">アルファーローション</h4>
						<p class="mainColor text-right">120ml 6,000円（税別）</p>
						<p class="lh_xl">フルーツ酸＜AHA>の働きで肌の表面に滞留している不要な角質を柔らかくし、剥がれやすくして、滑らかな肌へと導きます。</p>
					</li>
					<li>
						<div class="top_lotion_img bg-common mainBorderColor mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_lotion03.jpg');"></div>
						<h4 class="h4 mainColor mb10">エッセンシャルリキッド</h4>
						<p class="mainColor text-right">30ml 8,000円（税別）</p>
						<p class="lh_xl">セラミド・コラーゲン・ヒアルロン酸の美容成分が、肌に足りない潤い成分を浸透させ、イキイキとした素肌をつくります。</p>
					</li>
					<li>
						<div class="top_lotion_img bg-common mainBorderColor mb10" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_lotion04.jpg');"></div>
						<h4 class="h4 mainColor mb10">アップルジェル</h4>
						<p class="mainColor text-right">30ml 9,000円（税別）</p>
						<p class="lh_xl">女性ホルモン“エストロゲン”に似た働きをするイソフラボンが、乾燥した肌やシワ・タルミが気になる肌をハリのあるしっとりした肌に導きます。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>	
</section>
<!-- 化粧品 ここまで -->	

<!-- 養成講座 ここから -->
<section class="pd-common" id="top_course">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Course</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">セラピスト養成講座</h3>

				<p class="h3 mb20">好きや得意を仕事に</p>
				<p class="lh_xl mb30">今の仕事に疑問を感じている方や、子育てがひと段落したら好きなことや得意なことを仕事に<br>したいと考えている方のために、セラピスト養成講座を開講しています。<br>ご家族やご友人、大切な人のために技術を身につけたいと考えている方も歓迎です。<br>ラ・フィーネは、やりがいを持って働く女性を応援します。</p>

				<ul class="top_course_ul relative mb50">
					<li>
						<table class="top_cource_table">
							<tbody>
								<tr>
									<th><p class="h3">養成講座</p></th>
									<td>
										<p class="h4 lh_xl"><span class="pinkColor">◆</span>フェイシャルセラピストコース</p>
										<p class="h4 lh_xl"><span class="pinkColor">◆</span>ハンドセラピストコース</p>
										<p class="h4 lh_xl"><span class="pinkColor">◆</span>ヘッドセラピストコース</p>
									</td>
								</tr>
								<tr>
									<th><p class="h3">養成講座</p></th>
									<td>
										<p class="h3"><span class="pinkColor h2">15,000</span>円〜</p>
										<p class="mb0">※内容・受講期間・費用など、詳細はお問い合わせください。</p>
									</td>
								</tr>
							</tbody>
						</table>
					</li><!--
				 --><li>
						<div class="top_course_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_cource01.jpg');"></div>
					</li>
				</ul>

				<div class="text-center mb50">
					<a href="<?php echo home_url(); ?>/contact" class="pt_btn">養成講座に申し込む</a>
				</div>
			</div>
		</div>
	</div>	
</section>
<!-- 養成講座 ここまで -->
	

<div class="parallax parallax_diet_concept"
	 data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/diet_fv.jpg"
	 data-parallax-bg-position="center bottom"
	 data-parallax-speed="0.4"
	 data-parallax-direction="down"
>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center">
					<p class="engTitle h1 subColor text-center">Diet</p>
					<h3 class="mainTitle h2 mainColor text-center relative mb60">ラ・フィーネ式食べ痩せダイエット</h3>
					<p class="bold h4 mainColor mb30">サプリなし、運動なし！<br>しっかり食べながらダイエットしませんか？</p>
				</div>
				<div class="text-center mb50">
					<a href="<?php echo home_url(); ?>/diet" class="pt_btn">詳しく見る</a>
				</div>
			</div>
		</div>
	</div>
</div>
	
	
<section class="pd-common bgMainColor">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Voice</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">お客様の声</h3>
				
				<ul class="top_voice_ul ul-2 ul-sm-1 mb30 mb-xs-0">
					<li>
						<div class="top_voice_ul_inner relative">
							<h4 class="h3 mb10">「手」がすばらしいです！</h4>
							<p class="top_voice_name text_s bgLightBrownColor white mb20">50代Aさん</p>
							<p class="lh_xl">体、顔がスッキリしました。不眠ぎみでしたが、途中寝おちしてました。手がすばらしいです。</p>
						</div>
					</li>
					<li>
						<div class="top_voice_ul_inner relative">
							<h4 class="h3 mb10">肌が強くなりました。</h4>
							<p class="top_voice_name text_s bgLightBrownColor white mb20">60代Kさん</p>
							<p class="lh_xl">しばらく通ううちに、肌が強くなりました。</p>
						</div>
					</li>
					<li>
						<div class="top_voice_ul_inner relative">
							<h4 class="h3 mb10">通いたいサロンが見つかった！</h4>
							<p class="top_voice_name text_s bgLightBrownColor white mb20">40代Sさん</p>
							<p class="lh_xl">エステに通いたかったけど、お友達とはあまりエステサロンの話もしないから、なかなか見つけられなくて。通いたいサロンが見つかって嬉しい！</p>
						</div>
					</li>
					<li>
						<div class="top_voice_ul_inner relative">
							<h4 class="h3 mb10">ピンッとした気がします。</h4>
							<p class="top_voice_name text_s bgLightBrownColor white mb20">40代Yさん</p>
							<p class="lh_xl">今日は気持ちよくして頂いてありがとうございます。少しピンッとした気がします。また来させて頂きたいと思います。</p>
						</div>
					</li>
					<li>
						<div class="top_voice_ul_inner relative">
							<h4 class="h3 mb10">2、3日後によりしっとりしてきます。</h4>
							<p class="top_voice_name text_s bgLightBrownColor white mb20">40代Yさん</p>
							<p class="lh_xl">エステ直後より、2、3日後によりしっとりしてきます。</p>
						</div>
					</li>
					<li>
						<div class="top_voice_ul_inner relative">
							<h4 class="h3 mb10">ぐっすり眠れます！</h4>
							<p class="top_voice_name text_s bgLightBrownColor white mb20">60代Yさん</p>
							<p class="lh_xl">エステに来た日はぐっすり眠れます！</p>
						</div>
					</li>
					<li>
						<div class="top_voice_ul_inner relative">
							<h4 class="h3 mb10">とてもリフレッシュできました！</h4>
							<p class="top_voice_name text_s bgLightBrownColor white mb20">30代Mさん</p>
							<p class="lh_xl">短い時間だったのですが、とてもリフレッシュできました！やはり脳の疲れが取れると良いですね。定期的にやれたらと思います。月2回エステに通うということが叶い、とてもうれしいです！また来月もよろしくお願いします。</p>
						</div>
					</li>
					<li>
						<div class="top_voice_ul_inner relative">
							<h4 class="h3 mb10">息子から「お母さん綺麗になってるよ！」</h4>
							<p class="top_voice_name text_s bgLightBrownColor white mb20">30代Nさん</p>
							<p class="lh_xl">とても気持ち良く、最高に楽しい時間でした！息子にも、お母さん綺麗になってるよと言ってもらえました！ミユキさんに感謝です！また次回も楽しみにしています！よろしくお願いいたします！本当にありがとうございました。</p>
						</div>
					</li>
					<li>
						<div class="top_voice_ul_inner relative">
							<h4 class="h3 mb10">肩も首もうんと楽に</h4>
							<p class="top_voice_name text_s bgLightBrownColor white mb20">70代Sさん</p>
							<p class="lh_xl">本当に気持ちがよく、肩も首もうんと楽な感じがします。またお願いしたいです。</p>
						</div>
					</li>
					<li>
						<div class="top_voice_ul_inner relative">
							<h4 class="h3 mb10">気持ちいいエステにうっとり</h4>
							<p class="top_voice_name text_s bgLightBrownColor white mb20">50代Tさん</p>
							<p class="lh_xl">いつもながら、スッキリ、気持ちいいエステにうっとりです。肌の乾燥に気をつけますネ！</p>
						</div>
					</li>
					<li>
						<div class="top_voice_ul_inner relative">
							<h4 class="h3 mb10">小さくなった自分にビックリ</h4>
							<p class="top_voice_name text_s bgLightBrownColor white mb20">60代Aさん</p>
							<p class="lh_xl">今日はありがとうございました！とても快適でした。写メ送ってくださりありがとうございました。小さくなった自分にビックリです。いろいろと教えて頂いたこと実践しますね♫</p>
						</div>
					</li>
					<li>
						<div class="top_voice_ul_inner relative">
							<h4 class="h3 mb10">もっと早くくればよかった</h4>
							<p class="top_voice_name text_s bgLightBrownColor white mb20">30代Kさん</p>
							<p class="lh_xl">もっと早くくればよかった～ずいぶん前から友だちに聞いていたのに…</p>
						</div>
					</li>
				</ul>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common" id="top_question">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Question</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">よくあるご質問</h3>
				
				<dl class="top_qa_dl width980 flex mb20">
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q1</span>駐車場はありますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A1</span>1台ございます（無料）。P　ラ・フィーネと書いている看板の前にお停めください。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q2</span>当日予約できますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A2</span>当日のご予約承ります。営業時間中は朝10時から18時半まで受付けています。時間外のご予約も承りますので、お気軽にご相談ください。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q3</span>入会金や無理な勧誘はないですか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A3</span>ご安心ください。入会金や無理な勧誘などは一切ありません。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q4</span>クレジットカードは使えますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A4</span>VISAとMastercardでのお支払いが可能です。paypayでの支払いも対応しております。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q5</span>お友達と二人で行ってもいいですか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A5</span>セラピストが一人で施術をさせていただくため、エステはお一人ずつとなりますが、待合スペースがありますのでリラックスしてお待ちいただけます。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q6</span>コースが決まっていないのですが…</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A6</span>カウンセリングやクレンジングの際に、その日の肌やお体の状態、ご気分などをおうかがいした上でコースをお選びいただきます。ご予算などにも合わせてご提案いたしますので、お気軽にご来店ください。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q7</span>敏感肌でもエステできるでしょうか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A7</span>肌の状態を拝見し、カウンセリングさせていただいた上でご提案いたします。お気軽にご相談ください。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q8</span>着替えはありますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A8</span>お着替えはご用意しています。リラックスしてエステをお楽しみください。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q9</span>男性も予約できますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A9</span>お客様のご紹介の方のみ承ります。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q10</span>エステが終わった後に、メイクをしてもいいですか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A10</span>メイクスペースをご用意しています。お気に入りの道具をご持参ください。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q11</span>コース時間にプラスしてどれくらい時間がかかりますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A11</span>初回は、コースにプラス約1時間の余裕を持ってご来店ください。カルテのご記入やカウンセリング、足湯、お着替え、ティータイムなどが含まれます。お帰りのご希望時間があれば、お知らせください。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q12</span>化粧品の販売はしていますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A12</span>スキンケア化粧品”セレス・アルファーシリーズ”を取扱っています。施術後のアドバイスはさせていただいていますが、強引な化粧品販売はしていませんのでご安心ください。化粧品だけのご購入や郵送も可能です。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q13</span>まつげのエクステンションをしていても、施術を受けることはできますか？？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A13</span>可能です。ラ・フィーネで使用する化粧品にはオイルが含まれていませんので、まつげのエクステをしている方にもご利用いただけます。</div></dd>
				</dl>
				<dl class="top_qa_dl width980 flex mb20">	
					<dt class="mb20"><span class="top_qa_icon h2 white bgBrownColor">Q14</span>腰痛があるのですが、施術を受けることはできますか？</dt>
					<dd class="mb20"><div class="top_a_inner"><span class="top_qa_icon h2 white bgLightBrownColor">A14</span>可能です。当店ではサロン用のイタリア製可動式ベッドを導入し、腰や足にかかる負担を軽減しています。電動で微調整しながらお客さまの体にかかるご負担を最大限に軽減してまいりますが、施術中に不安があれば、いつでもお知らせください。</div></dd>
				</dl>
			</div>
		</div>
	</div>	
</section>
	

	
<section class="pd-common" id="top_access">
	<div class="container">
		<div class="row mb30">
			<div class="col-sm-12">
				<p class="engTitle h1 subColor text-center">Access</p>
				<h3 class="mainTitle h2 mainColor text-center relative mb60">アクセス</h3>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-6 mb-xs-20">
			    <dl class="dl-horizontal topAcessDl">
			         <dt>店舗名</dt>
			         <dd>毛穴エクストラクション&amp;アンチエイジングサロン<br class="pc">ラ・フィーネ</dd>
			         <dt>代表</dt>
			         <dd>江本美幸</dd>
			         <dt>電話</dt>
			         <dd>080-1906-1867</dd>
			         <dt>住所</dt>
			         <dd>〒700-0973<br>岡山市北区下中野708-105 ブルージュ下中野102</dd>
			         <dt>営業時間</dt>
			         <dd>10：00～18:30（最終受付）</dd>
			         <dt>駐車場</dt>
			         <dd>1台（無料）</dd>
			         <dt>定休日</dt>
			         <dd>金曜日</dd>
			    </dl>
			</div>
			<div class="col-sm-6">
				<img src="<?php echo get_template_directory_uri(); ?>/img/top_access.jpg" alt="フェイシャルケアとダイエットのエステサロン ラ・フィーネ">
			</div>
		</div>
	</div>	
</section>
	
<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3282.6039304876717!2d133.89471895109807!3d34.63944798035434!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x3554077cdcae1c75%3A0x830bdd6bdebcfd0a!2z44Op44O744OV44Kj44O844ON!5e0!3m2!1sja!2sjp!4v1575099338319!5m2!1sja!2sjp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
	
</main>

<?php get_footer(); ?>