<?php get_header(); ?>

<main>
	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="h1 engTitle bold mt40 mt-xs-20">RECRUIT</p>
				<h2 class="h2 bold">採用情報</h2>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common relative recruit_message" id="top_about">
	<div class="pt_grad absolute" data-aos="fade-right">
		<p class="engMainTitle engTitle white">MESSAGE</p>
		<h3 class="h3 white bold mb30 mb-xs-20">代表メッセージ</h3>
		<h4 class="h2 bold mb20">「音」で「人」を繋ぐ仕事</h4>
		<p class="white">コンサート・ライブ・イベント・コンベンションなど数多くのジャンルに対応し、<br>最新の音響機材での短時間で的確なセッティング、細やかで正確なオペレーティングを行います。<br>企画が生まれた瞬間から催事に関するご相談をお受けできますので、お客様のニーズやご予算に合わせた音響プランのご提案、クオリティの高い音響サービスのご提供が可能です。<br>【音】をただ聞こえるようにするというだけでなく、会場にいる誰もが【良い音】を感じ、<br>ステージの興奮や感動を、オーディエンス全体で共有できるような空間作りがJointlyの使命であると考えております。<br>Jointlyではこれらのコンセプトを具現化できる機材とスタッフをご用意してお待ちしております。</p>
	</div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12" data-aos="fade-left">
				<!--<div class="recruit_message_img bg-common" style="background-image: url('<?php //echo get_template_directory_uri();?>/img/recruit_message.jpg');"></div>-->
				<img src="<?php echo get_template_directory_uri();?>/img/recruit_message.jpg" class="recruit_message_img" alt="">
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center"><p class="engMainTitle engTitle white">WORKS</p></div>
				<h3 class="h3 bold white mb30 mb-xs-30 text-center">仕事内容</h3>
				<ul class="service01_ul ul-3 ul-xs-1">
					<li>
						<div class="service_ul_img bg-common mb10" style="background-image: url('<?php echo get_template_directory_uri();?>/img/recruit_work01.jpg');"></div>
						<h4 class="h4 bold text-center mb10">学園祭でのバンドステージ</h4>
						<p>バンド対応可能な音響機材はもちろん、当日演奏に必要な楽器レンタルのご相談も引き受けます。</p>
					</li>
					<li>
						<div class="service_ul_img bg-common mb10" style="background-image: url('<?php echo get_template_directory_uri();?>/img/recruit_work02.jpg');"></div>
						<h4 class="h4 bold text-center mb10">多目的野外広場でのコンサート</h4>
						<p>音響機材一式持込みのケースです。会場の広さ、観客動員数、音楽ジャンルなどによって使用する機材をセレクトし、現場に最適なセッティングで臨みます。</p>
					</li>
					<li>
						<div class="service_ul_img bg-common mb10" style="background-image: url('<?php echo get_template_directory_uri();?>/img/recruit_work03.jpg');"></div>
						<h4 class="h4 bold text-center mb10">各種ダンス教室などの発表会</h4>
						<p>市民会館等の各種ホールで開催される発表会など、会場機材へ弊社持込み機材（ミキサーや再生機など）を繋ぎ込み、オペレートする場合もあります。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common" id="recruit_interview">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center-xs"><p class="engMainTitle engTitle white">INTERVIEW</p></div>
				<div class="text-center-xs"><h3 class="h3 bold mb20">パートナーメッセージ</h3></div>
			</div>
		</div>
	</div>
	<div class="pt_white_area_outer relative mb50 mb-xs-0">
		<div class="container">
			<div class="row">
				<div class="col-sm-12">
					<div class="pt_white_area relative mb50" data-aos="fade-up">
						<div class="pt_white_area_inner">
							<div class="text-center-xs"><h4 class="h2 mainColor bold mb20">「音」で「人」を繋ぐ仕事</h4></div>
							<p class="grayColor bold mb10">株式会社◯◯音響</p>
							<p class="h3 grayColor bold mb50">田中 一郎</p>
							<p class="grayColor bold mb20">Q. ジョイントリーの魅力とは？</p>
							<p class="grayColor">コンサート・ライブ・イベント・コンベンションなど数多くのジャンルに対応し、最新の音響機材での短時間で的確なセッティング、細やかで正確なオペレーティングを行います。<br>企画が生まれた瞬間から催事に関するご相談をお受けできますので、お客様のニーズやご予算に合わせた音響プランのご提案、クオリティの高い音響サービスのご提供が可能です。</p>
						</div>
					</div>
				</div>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common relative mb50" data-aos="fade-up">
	<div class="pt_grad recruit_description_bg absolute"></div>
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="recruit_description_area">
					<p class="engMainTitle engTitle white">DESCRIPTION</p>
					<h3 class="h3 white bold mb20">募集要項</h3>
					<table class="company_profile_table width720">
						<tbody>
							<tr>
								<th>勤務時間</th>
								<td>平日・土曜日<br>9:00 ~ 12:00 / 15:00 ~ 20:00</td>
							</tr>
							<tr>
								<th>休日・休暇</th>
								<td>週休 1.5 日または週休 2 日制(どちらかお選びいただけます。)<br>年末年始、GW、夏季休暇</td>
							</tr>
							<tr>
								<th>募集資格</th>
								<td>特になし</td>
							</tr>
							<tr>
								<th>雇用形態</th>
								<td>正社員(常勤)</td>
							</tr>
							<tr>
								<th>待遇</th>
								<td>27万~40万(試用期間3ヶ月は21万~) <br>※学生アルバイトはご相談ください<br>※待遇は能力によりどんどん上がります</td>
							</tr>
							<tr>
								<th>賞与</th>
								<td>年2回(各1ヶ月分支給) ※能力・業績による</td>
							</tr>
							<tr>
								<th>福利厚生</th>
								<td>社会保険・厚生年金・労災保険・雇用保険、<br>セミナー・研修費用補助制度あり</td>
							</tr>
							<tr>
								<th>通勤</th>
								<td>駐車場完備、マイカー・バイク通勤OKです(車通勤は要相談)</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>		
</section>
	
<div class="container">
	<div class="row">
		<div class="col-sm-12">
			<div class="text-center">
					<a href="<?php echo home_url(); ?>/contact" class="pt_btn h3 engTitle">JOIN</a>
				</div>
		</div>
	</div>	
</div>

	
</main>

<?php get_footer(); ?>