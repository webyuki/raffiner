<?php
/*
Template Name: わたしたちのできること
*/
?>

<?php get_header(); ?>

<section class="under_fv bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/under_detail_fv.png'); background-color: #ebebde;">
	<h2 class="under_fv_title">作物について</h2>	
</section>

<section class="pd-common menu_pro mb20" id="top_pro" style="background-color: #ebebde;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 mb30">
				<div class="text-center text-center-xs"><h3 class="pt_title02">倉ファームの作っている<span>野菜</span></h3></div>
				<p class="text-center mb50">倉ファームは自然豊かな環境の中、水にこだわり昔ながらのスタイルで<br>安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
				
				<ul class="ul-2 ul-xs-1 top_pro_ul text-center" data-aos="fade-up">
					<li>
						<a href="<?php echo home_url(); ?>/omakase">
							<div class="top_pro_ul_img_outer">
								<div class="top_pro_ul_img_inner">
									<div class="top_pro_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_pro_atago.jpg');"></div>
								</div>
								<h4 class="top_pro_ul_name"><span>あたご梨</span></h4>
								<img class="top_pro_ul_icon" src="<?php echo get_template_directory_uri(); ?>/img/top_pro_icon.png" alt="あたご梨">
							</div>
							<p class="top_pro_ul_date">収穫時期</p>
							<p class="top_pro_ul_txt">11月中旬~12月中旬</p>
						</a>
					</li>
                    <!--
                    <li>
						<a href="<?php echo home_url(); ?>/omakase">
							<div class="top_pro_ul_img_outer">
								<div class="top_pro_ul_img_inner">
									<div class="top_pro_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_pro_atago.jpg');"></div>
								</div>
								<h4 class="top_pro_ul_name"><span>あたご梨</span></h4>
								<img class="top_pro_ul_icon" src="<?php echo get_template_directory_uri(); ?>/img/top_pro_icon.png" alt="あたご梨">
							</div>
							<p class="top_pro_ul_date">収穫時期</p>
							<p class="top_pro_ul_txt">11月中旬~12月中旬</p>
						</a>
					</li>
                    -->
				</ul>
			</div>
			<div class="col-sm-12">
				<div class="text-center text-center-xs"><p class="pt_title01">その他にも、色んな野菜作ってます!</p></div>
				<ul class="top_pro_slider">
					<li>
						<div class="top_pro_slider_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_pro01.jpg');"></div>
					</li>
					<li>
						<div class="top_pro_slider_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_pro02.jpg');"></div>
					</li>
					<li>
						<div class="top_pro_slider_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_pro03.jpg');"></div>
					</li>
					<li>
						<div class="top_pro_slider_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_pro04.jpg');"></div>
					</li>
					<li>
						<div class="top_pro_slider_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_pro05.jpg');"></div>
					</li>
				</ul>
				
				<div class="text-center text-center-xs"><h3 class="pt_title02"><span>お得なセット販売</span><br class="visible-xs">も行っています</h3></div>
				<p class="text-center mb50">倉ファームは自然豊かな環境の中、水にこだわり昔ながらのスタイルで<br>安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
				
				<ul class="ul-2 ul-xs-1 top_pro_ul mb0 text-center" data-aos="fade-up">
					<li>
						<a href="<?php echo home_url(); ?>/omakase">
							<div class="top_pro_ul_img_outer">
								<div class="top_pro_ul_img_inner">
									<div class="top_pro_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_pro_osusume.jpg');"></div>
								</div>
								<h4 class="top_pro_ul_name"><span>おまかせパック</span></h4>
							</div>
							<p class="top_pro_ul_date">収穫時期</p>
							<p class="top_pro_ul_txt">通年</p>
						</a>
						<p class="">倉ファームは自然豊かな環境の中、水にこだわり昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
					</li>
                    <!--
                    <li>
						<a href="<?php echo home_url(); ?>/omakase">
							<div class="top_pro_ul_img_outer">
								<div class="top_pro_ul_img_inner">
									<div class="top_pro_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_pro_osusume.jpg');"></div>
								</div>
								<h4 class="top_pro_ul_name"><span>おまかせパック</span></h4>
							</div>
							<p class="top_pro_ul_date">収穫時期</p>
							<p class="top_pro_ul_txt">通年</p>
						</a>
						<p class="">倉ファームは自然豊かな環境の中、水にこだわり昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
					</li>
                    -->
				</ul>
			</div>
		</div>
	</div>
</section>

<?php get_footer(); ?>


















<div class="pageFv">
	<div class="container">
		<?php get_template_part( 'parts/page-fv' ); ?>
	</div>
</div>
<?php $slug = $post -> post_name; ?>

<section class="area_single">
	<div class="container">
		<?php get_template_part( 'parts/breadcrumb' ); ?>				
		<div class="row">
			<div class="col-sm-9">
				<div class="entry">
					<?php
						$args = array(
							'post_type' =>  'menu', // 投稿タイプを指定
							'posts_per_page' => 1, // 表示するページ数
							'order'=>'ASC',
							'orderby'=>'menu_order',
							'tax_query' => array(
								array(
									'taxonomy' => 'menu_tax',
									'field' => 'slug',
									'terms' => $slug,
									/*'operator'  => 'NOT IN'*/
								)	
							),								
						);
						$wp_query = new WP_Query( $args ); // クエリの指定 	
						while ( $wp_query->have_posts() ) : $wp_query->the_post();
							//ここに表示するタイトルやコンテンツなどを指定 
						get_template_part('content','post'); 
				
						endwhile;
						wp_reset_postdata(); //忘れずにリセットする必要がある
					?>		
				<?php get_template_part( 'parts/postlink' ); ?>				
					
				</div>
				<?php //get_template_part( 'parts/pagenation' ); ?>				
			</div>
			<div class="col-sm-3">
				<?php //get_sidebar(); ?>
                <?php get_sidebar('menu'); ?>
                
   			</div>
		</div>
	</div>
</section>
<?php get_footer(); ?>




