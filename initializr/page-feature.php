<?php get_header(); ?>
<main style="background-color: #ebeae3;">

<section class="under_fv" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature_fv.png');">
	<div class="under_fv_txtarea">
		<h2 class="under_fv_jp h_mincho">当院の特徴</h2>
		<p class="under_fv_eng">Feature</p>
	</div>
</section>
	
<section class="pd-common mt70 mt-sm-40 mt-xs-0 mb60 mb-sm-0 mb-xs-0 service_content feature_content feature_content01" id="" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature01.png');">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="ul-2 ul-xs-1 service_ul mb0 mb-xs-0">
					<li>
						<img class="visible-xs" src="<?php echo get_template_directory_uri(); ?>/img/feature01.png" alt="院内処方">
					</li>
					<li>
						<div class="service_title_outer">
							<h3 class="service_title_jp main_color h_mincho">院内処方</h3>
							<p class="service_title_eng main_color">Internal medicine</p>
						</div>
						<p class="text-justify service_txt">お薬を医療機関の薬局で調剤し、直接お持ち帰りいただくのが院内処方です。お薬をもらいに調剤薬局まで足を運ぶ必要がなく、お会計も一度で済みます。<br><br>また調剤薬局等への手数料等がないため、自己負担金額が少ないというメリットがあります。<br><br>当医院では医療上の安全を確保、また丁寧な服薬指導をするため、特にリスクのある薬剤はきちんと服薬管理ができる院内処方でお出ししています。なお、特殊なお薬の場合や院外処方箋をご希望の方には処方箋を発行いたします。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common mb60 mb-sm-0 mb-xs-0 service_content service_content02 feature_content feature_content02" id="" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature02.png');">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="ul-2 ul-xs-1 service_ul feature_service_ul mb0 mb-xs-0">
					<li>
						<div class="service_title_outer">
							<h3 class="service_title_jp main_color h_mincho">ジェネリック医薬品</h3>
							<p class="service_title_eng main_color">Internal medicine</p>
						</div>
						<p class="text-justify service_txt">ジェネリック医薬品を希望される場合、すでに定評のあるジェネリック薬品は採用し、患者さん負担の節約に努めて参ります。<br><br>ただジェネリック薬品に代わりとなるものがない場合や、効きめと安全性を考慮した場合、先発品を使用することもあります。想定した治療効果をきちんとあげるためです。</p>
					</li>
					<li>
						<img class="visible-xs" src="<?php echo get_template_directory_uri(); ?>/img/feature02.png" alt="ジェネリック医薬品">
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common mb60 mb-sm-0 mb-xs-0 service_content feature_content feature_content03" id="" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature03.png');">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="ul-2 ul-xs-1 service_ul mb0 mb-xs-0">
					<li>
						<img class="visible-xs" src="<?php echo get_template_directory_uri(); ?>/img/feature03.png" alt="医院連携">
					</li>
					<li>
						<div class="service_title_outer">
							<h3 class="service_title_jp main_color h_mincho">医院連携</h3>
							<p class="service_title_eng main_color">Internal medicine</p>
						</div>
						<p class="text-justify service_txt">患者様の症状により、下記病院と連携して症状の改善、健康状態の回復に臨みます。詳細につきましては、当院医師へお問い合わせください。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="about_ul_area about_ul_area_feature">
					<h4 class="about_ul_title h_mincho">連携医院一覧</h4>
					<table class="feature_list_table">
						<tbody>
							<tr>
								<th><p class="feature_list_name mb0">川崎医科大学総合医療センター</p></th>
								<td><p class="feature_list_add mb0">岡山市北区中山下2丁目6−1</p></td>
							</tr>
							<tr>
								<th><p class="feature_list_name mb0">川崎医科大学総合医療センター</p></th>
								<td><p class="feature_list_add mb0">岡山市北区中山下2丁目6−1</p></td>
							</tr>
							<tr>
								<th><p class="feature_list_name mb0">川崎医科大学総合医療センター</p></th>
								<td><p class="feature_list_add mb0">岡山市北区中山下2丁目6−1</p></td>
							</tr>
							<tr>
								<th><p class="feature_list_name mb0">川崎医科大学総合医療センター</p></th>
								<td><p class="feature_list_add mb0">岡山市北区中山下2丁目6−1</p></td>
							</tr>
							<tr>
								<th><p class="feature_list_name mb0">川崎医科大学総合医療センター</p></th>
								<td><p class="feature_list_add mb0">岡山市北区中山下2丁目6−1</p></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common" style="background-color: #cbdecb;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center text-center-xs"><p class="pt_title_eng">History</p></div>
				<div class="text-center text-center-xs mb100 mb-xs-60"><h3 class="pt_title_jp h_mincho">沿革</h3></div>
				<ul class="ul-2 ul-xs-1 feature_history_ul">
					<li>
						<p class="feature_history_date h_mincho">大正12年 <span>平松醫院を開業</span></p>
						<img class="feature_history_img" src="<?php echo get_template_directory_uri(); ?>/img/feature_history01.jpg" alt="平松醫院を開業">
						<p>理事長の祖父、故平松直が北区磨屋町の旅館跡に「平松醫院」皮膚泌尿器科を開業</p>
					</li>
					<li>
						<p class="feature_history_date h_mincho">大正14年 <span>現所在地に移転</span></p>
					</li>
				</ul>
				<ul class="ul-2 ul-xs-1 feature_history_ul">
					<li>
						<p class="feature_history_date h_mincho">昭和20年 <span>医院全焼</span></p>
						<p>岡山空襲の戦災により医院全焼。<br>現所在地に仮診療所を開設</p>
					</li>
					<li>
						<p class="feature_history_date h_mincho">昭和27年 <span>肛門科を併設</span></p>
						<img class="feature_history_img" src="<?php echo get_template_directory_uri(); ?>/img/feature_history02.jpg" alt="肛門科を併設">
						<p>理事長の父、平松照雄が副院長就任。<br>肛門科を併設</p>
					</li>
				</ul>
				<ul class="ul-2 ul-xs-1 feature_history_ul">
					<li>
						<p class="feature_history_date h_mincho">昭和34年 <span>全面改修</span></p>
						<img class="feature_history_img feature_history_img03" src="<?php echo get_template_directory_uri(); ?>/img/feature_history03.jpg" alt="全面改修">
						<p>旧施設を解体し、現在の「平松醫院」に全面改修</p>
					</li>
					<li>
						<p class="feature_history_date h_mincho">昭和39年 <span>２代目院長が継承</span></p>
						<p>２代目院長、平松照雄が継承</p>
					</li>
				</ul>
				<ul class="ul-2 ul-xs-1 feature_history_ul">
					<li>
						<p class="feature_history_date h_mincho">平成15年 <span>内科・呼吸器科を併設</span></p>
						<p>平松順一が勤務開始<br>内科・呼吸器科を併設</p>
					</li>
					<li>
						<p class="feature_history_date h_mincho">平成27年 <span>３代目院長が継承</span></p>
						<p>３代目院長、平松順一が継承<br>現在に至る</p>
					</li>
				</ul>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<h3 class="service_inspection_maintitle mb50 h_mincho">多くの患者様の声を<br class="visible-xs">頂いております。</h3>
				<ul class="ul-4 ul-xs-1 feature_voice_ul">
					<li>
						<div class="feature_voice_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature_voice01.png');"></div>
						<h4 class="feature_voice_title h_mincho">丁寧に診察して頂いて<br>大変助かりました</h4>
						<p>定期的な検査の中で、生活・食事指導をし、症状や健康状態により薬物療法を行います。</p>
					</li>
					<li>
						<div class="feature_voice_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature_voice02.png');"></div>
						<h4 class="feature_voice_title h_mincho">丁寧に診察して頂いて<br>大変助かりました</h4>
						<p>定期的な検査の中で、生活・食事指導をし、症状や健康状態により薬物療法を行います。</p>
					</li>
					<li>
						<div class="feature_voice_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature_voice03.png');"></div>
						<h4 class="feature_voice_title h_mincho">丁寧に診察して頂いて<br>大変助かりました</h4>
						<p>定期的な検査の中で、生活・食事指導をし、症状や健康状態により薬物療法を行います。</p>
					</li>
					<li>
						<div class="feature_voice_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/feature_voice04.png');"></div>
						<h4 class="feature_voice_title h_mincho">丁寧に診察して頂いて<br>大変助かりました</h4>
						<p>定期的な検査の中で、生活・食事指導をし、症状や健康状態により薬物療法を行います。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
	

</main>






<?php get_footer(); ?>