<?php get_header(); ?>

<?php 
	while ( have_posts() ) : the_post();
?>

<?php 
	// テキスト・ウィジェット関係
	$demand = get_field("demand");
	$advice = get_field("advice");
	$address = get_field("address");
	$period = get_field("period");
	$age = get_field("age");
	$price = get_field("price");
	$item = get_field("item");
	$exam_1_text = get_field("exam_1_text");
	$exam_2_text = get_field("exam_2_text");
	$voice_text = get_field("voice_text");
	$imgs = get_field("imgs");
	// メイン画像
	if ( has_post_thumbnail() ) {
		$image_id = get_post_thumbnail_id();
		$image_url = wp_get_attachment_image_src ($image_id,'large',true);
	} else {
	}  
?>


<main style="background-color: #eae7e5;">
	
<section class="under_fv bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_fv.jpg');">
	<div class="under_fv_titlearea">
		<p class="pt_title_eng text-center text-center-xs mb0">WORKS</p>
		<h2 class="pt_title_jp text-center text-center-xs mb0">施工事例</h2>	
	</div>
</section>

<section class="pd-common pt0" style="background-color: #ebe8e6;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="detail_title"><?php the_title(); ?></p>
				<div class="text-center mb30">
                
                    <?php 
                        if ($terms = get_the_terms($post->ID, 'works_cate')) {
                            foreach ( $terms as $term ) {
                                echo '<p class="detail_add">' . esc_html($term->name) . '</p>';
                                $term_slug = $term->slug;
                            }
                        }
                    ?>
                    <?php 
                        if ($terms = get_the_terms($post->ID, 'works_area')) {
                            foreach ( $terms as $term ) {
                                echo '<p class="detail_tag">' . esc_html($term->name) . '</p>';
                                $term_slug = $term->slug;
                            }
                        }
                    ?>
                
				</div>
				<div class="detail_main_imgarea">
                
                    <?php if (has_post_thumbnail()):?>
                        <?php 
                            // アイキャッチ画像のIDを取得
                            $thumbnail_id = get_post_thumbnail_id();
                            // mediumサイズの画像内容を取得（引数にmediumをセット）
                            $eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
                            $eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
                        ?>
                            <div class="detail_main_img bg-common" style="background-image: url('<?php echo $eye_img_s[0];?>');"></div>
                        <?php else: ?>
                            <div class="detail_main_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/sample01.png');"></div>
                    <?php endif; ?>
				</div>
			</div>
		</div>
	</div>
	<div class="container detail_txtarea mb50">
		<div class="row">
			<div class="col-sm-6">
				<div class="detail_txt mb-xs-30">
                    <div class="entry">
                        <?php the_content();?>
                    </div>
                </div>
			</div>
			<div class="col-sm-6">
				<div class="detail_table_outer">
					<p class="detail_table_title">DATA</p>
					<table class="detail_table">
						<tbody>
							<tr>
								<th>施工エリア</th>
								<td>
                                
                                    <?php 
                                        if ($terms = get_the_terms($post->ID, 'works_area')) {
                                            foreach ( $terms as $term ) {
                                                echo '<span>' . esc_html($term->name) . '</span>';
                                                $term_slug = $term->slug;
                                            }
                                        }
                                    ?>
                                
                                </td>
							</tr>
							<tr>
								<th>施工カテゴリ</th>
								<td>
                                
                                    <?php 
                                        if ($terms = get_the_terms($post->ID, 'works_cate')) {
                                            foreach ( $terms as $term ) {
                                                echo '<span>' . esc_html($term->name) . '</span>';
                                                $term_slug = $term->slug;
                                            }
                                        }
                                    ?>
                                
                                </td>
							</tr>
							<tr>
								<th>投稿日</th>
								<td><?php the_time('Y年n月j日'); ?></td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="container detail_txtarea">
		<div class="row">
			<div class="col-sm-12">
				<p class="detail_before_title">BEFORE</p>
				<ul class="detail_before_ul ul-4 ul-xs-2">
                
                    <?php $i = 1; ?>
                    <?php while (get_field('img_0'.$i)) : ?>
                        <li>
                            <a href="<?php the_field('img_0'.$i); ?>" data-lightbox="before" data-title="<?php the_field('img_text_0'.$i); ?>">
                                <div class="detail_before_ul_img_outer">
                                    <div class="detail_before_ul_img bg-common" style="background-image: url('<?php the_field('img_0'.$i); ?>');"></div>
                                </div>
                                <p class="detail_before_ul_title"><?php the_field('img_text_0'.$i); ?></p>
                            </a>
                        </li>
                    
                    <?php $i++; ?>
                    <?php endwhile; ?>


				</ul>
			</div>
			<div class="col-sm-12">
				<p class="detail_before_title">AFTER</p>
				<ul class="detail_before_ul ul-4 ul-xs-2">
                    <?php $i = 1; ?>
                    <?php while (get_field('img_after_0'.$i)) : ?>
                        <li>
                            <a href="<?php the_field('img_after_0'.$i); ?>" data-lightbox="before" data-title="<?php the_field('img_after_text_0'.$i); ?>">
                                <div class="detail_before_ul_img_outer">
                                    <div class="detail_before_ul_img bg-common" style="background-image: url('<?php the_field('img_after_0'.$i); ?>');"></div>
                                </div>
                                <p class="detail_before_ul_title"><?php the_field('img_after_text_0'.$i); ?></p>
                            </a>
                        </li>
                    
                    <?php $i++; ?>
                    <?php endwhile; ?>
				</ul>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common" id="top_works" style="background-color: #3a3c49;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center text-center-xs">
					<p class="pt_title_eng">OTHERS</p>
					<h3 class="pt_title_jp">他の事例を見る</h3>
				</div>
			</div>
			<div class="col-sm-12">
				<ul class="top_works_ul ul-2 ul-xs-1 mb50">
                
                    <?php
                                $args = array(
                                'post_type' => 'works', //投稿タイプ名
                                'posts_per_page' => 2, //出力する記事の数
                                'tax_query' => array(
                                array(
                                'taxonomy' => 'works_cate', //タクソノミー名
                                'field' => 'slug',
                                'terms' => $term_slug //タームのスラッグ
                                )
                                )
                                );
                            ?>
                            <?php
                            $myposts = get_posts( $args );
                            foreach ( $myposts as $post ) : setup_postdata( $post );
                            ?>

                    <?php get_template_part('content-post-works-archive'); ?>

                    <?php endforeach; ?>
                
				</ul>
				<div class="text-center text-center-xs mb20"><a href="<?php echo home_url(); ?>/works" class="pt_btn">事例一覧に戻る</a></div>
			</div>
		</div>
	</div>
</section>

</main>








<?php 
	endwhile;
?>	


<?php get_footer(); ?>


