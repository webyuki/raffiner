<?php get_header(); ?>

<main style="background-color: #eae7e5;">
	
<section class="under_fv bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_fv.jpg');">
	<div class="under_fv_titlearea">
		<p class="pt_title_eng text-center text-center-xs mb0">WORKS</p>
		<h2 class="pt_title_jp text-center text-center-xs mb0">施工事例</h2>	
	</div>
</section>

<section class="pd-common pt0" style="background-color: #ebe8e6;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="detail_title">倉敷市のO様邸 雨樋の架替工事</p>
				<div class="text-center mb30">
					<p class="detail_add">倉敷市</p>
					<p class="detail_tag">雨樋の架替工事</p>
				</div>
				<div class="detail_main_imgarea">
					<div class="detail_main_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg');"></div>
				</div>
			</div>
		</div>
	</div>
	<div class="container detail_txtarea mb50">
		<div class="row">
			<div class="col-sm-6">
				<p class="detail_subtitle">屋根の状態を把握して、板金屋根への変更を提案</p>
				<p class="detail_txt mb-xs-30">K様のお宅を現調させていただいた時に一番心配になったのが屋根の状態です。<br>カラーベストがミルフィーユ状態に劣化していました。<br>K様もこのままではいけないと認知されており、板金の屋根にやりかえる方法での見積もりをさせて頂きました。<br><br>他社との比較もして頂き、他社と弊社の工事内容の違いや弊社としての提案では作業方法や使用材料を変えて安くすることはできないラインのご説明をさせて頂きました。<br>悩まれた結果、見積の内容が細かくて施工もしっかりとしてくれるだろうと工事を依頼して頂きました。<br>想いが伝わって良かったです！</p>
			</div>
			<div class="col-sm-6">
				<div class="detail_table_outer">
					<p class="detail_table_title">DATA</p>
					<table class="detail_table">
						<tbody>
							<tr>
								<th>施工エリア</th>
								<td>岡山県岡山市北区磨屋町</td>
							</tr>
							<tr>
								<th>施工カテゴリ</th>
								<td>外壁塗装</td>
							</tr>
							<tr>
								<th>投稿日</th>
								<td>2019.10.01</td>
							</tr>
						</tbody>
					</table>
				</div>
			</div>
		</div>
	</div>
	<div class="container detail_txtarea">
		<div class="row">
			<div class="col-sm-12">
				<p class="detail_before_title">BEFORE</p>
				<ul class="detail_before_ul ul-4 ul-xs-2">
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg" data-lightbox="before" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title">下葺材撤去</p>
						</a>
					</li>
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg" data-lightbox="before" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title">下葺材撤去</p>
						</a>
					</li>
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg" data-lightbox="before" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title">下葺材撤去</p>
						</a>
					</li>
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg" data-lightbox="before" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title">下葺材撤去</p>
						</a>
					</li>
				</ul>
			</div>
			<div class="col-sm-12">
				<p class="detail_before_title">AFTER</p>
				<ul class="detail_before_ul ul-4 ul-xs-2">
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg" data-lightbox="after" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title">下葺材撤去</p>
						</a>
					</li>
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg" data-lightbox="after" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title">下葺材撤去</p>
						</a>
					</li>
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg" data-lightbox="after" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title">下葺材撤去</p>
						</a>
					</li>
					<li>
						<a href="<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg" data-lightbox="after" data-title="下葺材撤去">
							<div class="detail_before_ul_img_outer">
								<div class="detail_before_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_sample.jpg');"></div>
							</div>
							<p class="detail_before_ul_title">下葺材撤去</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>
	
<section class="pd-common" id="top_works" style="background-color: #3a3c49;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="text-center text-center-xs">
					<p class="pt_title_eng">OTHERS</p>
					<h3 class="pt_title_jp">他の事例を見る</h3>
				</div>
			</div>
			<div class="col-sm-12">
				<ul class="top_works_ul ul-2 ul-xs-1 mb50">
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_ul_img_outer">
								<div class="top_works_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_works_sample.jpg');"></div>
							</div>
							<div class="top_works_ul_txtarea matchHeight">
								<h5 class="top_works_ul_title">倉敷市のO様邸 雨樋の架替工事</h5>
								<p class="top_works_ul_add">倉敷市</p>
								<p class="top_works_ul_tag">雨樋の架替工事</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_ul_img_outer">
								<div class="top_works_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_works_sample.jpg');"></div>
							</div>
							<div class="top_works_ul_txtarea matchHeight">
								<h5 class="top_works_ul_title">倉敷市のO様邸 雨樋の架替工事</h5>
								<p class="top_works_ul_add">倉敷市</p>
								<p class="top_works_ul_tag">雨樋の架替工事</p>
							</div>
						</a>
					</li>
				</ul>
				<div class="text-center text-center-xs mb20"><a href="<?php echo home_url(); ?>/works" class="pt_btn">事例一覧に戻る</a></div>
			</div>
		</div>
	</div>
</section>

</main>






<?php get_footer(); ?>