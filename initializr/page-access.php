<?php get_header(); ?>
<main>
	
<section class="under_fv" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/access_fv.png');">
	<div class="under_fv_txtarea">
		<h2 class="under_fv_jp h_mincho">アクセス・診療時間</h2>
		<p class="under_fv_eng">Access</p>
	</div>
</section>

<section class="pd-common" style="background-color: #e8e7e0;">
	<div class="container">
		<div class="row mb50">
			<div class="col-sm-6">
				<h3 class="service_inspection_maintitle access_maintitle h_mincho mb-xs-10">アクセス</h3>
				<table class="access_table mb-xs-20">
					<tbody>
						<tr>
							<th>所在地</th>
							<td>〒700-0826<br>岡山県岡山市北区磨屋町9-5<br>(医院裏手に駐車場 2 台あり)</td>
						</tr>
						<tr>
							<th>TEL</th>
							<td>086-232-1070</td>
						</tr>
						<tr>
							<th>FAX</th>
							<td>086-232-1070</td>
						</tr>
						<tr>
							<th>最寄り駅等</th>
							<td>◯◯停留所から徒歩3分<br>◯◯停留所から徒歩3分<br>◯◯停留所から徒歩3分</td>
						</tr>
					</tbody>
				</table>
			</div>
			<div class="col-sm-6">
				<img src="<?php echo get_template_directory_uri(); ?>/img/access_map.jpg" alt="岡山県岡山市北区磨屋町9-5">
			</div>
		</div>
		<div class="row mb100 mb-xs-50">
			<div class="col-sm-12">
				<iframe src="https://www.google.com/maps/embed?pb=!1m18!1m12!1m3!1d3281.6815126641554!2d133.92257991553979!3d34.662745380444306!2m3!1f0!2f0!3f0!3m2!1i1024!2i768!4f13.1!3m3!1m2!1s0x35540635862da95f%3A0x901c044d11842e94!2z44CSNzAwLTA4MjYg5bKh5bGx55yM5bKh5bGx5biC5YyX5Yy656Oo5bGL55S677yZ4oiS77yV!5e0!3m2!1sja!2sjp!4v1568292904759!5m2!1sja!2sjp" width="100%" height="450" frameborder="0" style="border:0;" allowfullscreen=""></iframe>
			</div>
		</div>
		<div class="row">
			<div class="col-sm-12">
				<h3 class="service_inspection_maintitle h_mincho mb80 mb-xs-30">診療時間</h3>
				<table class="table table-bordered calendar text-center mb50">
					<tbody>
						<tr>
							<th>診療時間</th>
							<th>月</th>
							<th>火</th>
							<th>水</th>
							<th>木</th>
							<th>金</th>
							<th>土</th>
							<th>日</th>
						</tr>
						<tr>
							<td class="time">9:00〜13:00</td>
							<td>◯</td>
							<td>ー</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>ー</td>
							<td>ー</td>
						</tr>
						<tr>
							<td class="time">16:00〜18:00</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>◯</td>
							<td>ー</td>
							<td>ー</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>
</section>

</main>






<?php get_footer(); ?>