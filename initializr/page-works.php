<?php get_header(); ?>

<main style="background-color: #eae7e5;">
	
<section class="under_fv bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_fv.jpg');">
	<div class="under_fv_titlearea">
		<p class="pt_title_eng text-center text-center-xs mb0">WORKS</p>
		<h2 class="pt_title_jp text-center text-center-xs mb0">施工事例</h2>	
	</div>
</section>

<section class="pd-common pt0" id="top_works" style="background-color: #ebe8e6;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="list_link">
					<li>
						<a href="">雨樋の架替工事</a>
					</li>
					<li>
						<a href="">屋根の葺き替え工事</a>
					</li>
					<li>
						<a href="">車庫の取替</a>
					</li>
					<li>
						<a href="">その他</a>
					</li>
				</ul>
				<ul class="top_works_ul ul-2 ul-xs-1 mb50">
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_ul_img_outer">
								<div class="top_works_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_works_sample.jpg');"></div>
							</div>
							<div class="top_works_ul_txtarea matchHeight">
								<h5 class="top_works_ul_title">倉敷市のO様邸 雨樋の架替工事</h5>
								<p class="top_works_ul_add">倉敷市</p>
								<p class="top_works_ul_tag">雨樋の架替工事</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_ul_img_outer">
								<div class="top_works_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_works_sample.jpg');"></div>
							</div>
							<div class="top_works_ul_txtarea matchHeight">
								<h5 class="top_works_ul_title">倉敷市のO様邸 雨樋の架替工事</h5>
								<p class="top_works_ul_add">倉敷市</p>
								<p class="top_works_ul_tag">雨樋の架替工事</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_ul_img_outer">
								<div class="top_works_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_works_sample.jpg');"></div>
							</div>
							<div class="top_works_ul_txtarea matchHeight">
								<h5 class="top_works_ul_title">倉敷市のO様邸 雨樋の架替工事</h5>
								<p class="top_works_ul_add">倉敷市</p>
								<p class="top_works_ul_tag">雨樋の架替工事</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_ul_img_outer">
								<div class="top_works_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_works_sample.jpg');"></div>
							</div>
							<div class="top_works_ul_txtarea matchHeight">
								<h5 class="top_works_ul_title">倉敷市のO様邸 雨樋の架替工事</h5>
								<p class="top_works_ul_add">倉敷市</p>
								<p class="top_works_ul_tag">雨樋の架替工事</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_ul_img_outer">
								<div class="top_works_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_works_sample.jpg');"></div>
							</div>
							<div class="top_works_ul_txtarea matchHeight">
								<h5 class="top_works_ul_title">倉敷市のO様邸 雨樋の架替工事</h5>
								<p class="top_works_ul_add">倉敷市</p>
								<p class="top_works_ul_tag">雨樋の架替工事</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_ul_img_outer">
								<div class="top_works_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_works_sample.jpg');"></div>
							</div>
							<div class="top_works_ul_txtarea matchHeight">
								<h5 class="top_works_ul_title">倉敷市のO様邸 雨樋の架替工事</h5>
								<p class="top_works_ul_add">倉敷市</p>
								<p class="top_works_ul_tag">雨樋の架替工事</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_ul_img_outer">
								<div class="top_works_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_works_sample.jpg');"></div>
							</div>
							<div class="top_works_ul_txtarea matchHeight">
								<h5 class="top_works_ul_title">倉敷市のO様邸 雨樋の架替工事</h5>
								<p class="top_works_ul_add">倉敷市</p>
								<p class="top_works_ul_tag">雨樋の架替工事</p>
							</div>
						</a>
					</li>
					<li>
						<a href="<?php echo home_url(); ?>/detail">
							<div class="top_works_ul_img_outer">
								<div class="top_works_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_works_sample.jpg');"></div>
							</div>
							<div class="top_works_ul_txtarea matchHeight">
								<h5 class="top_works_ul_title">倉敷市のO様邸 雨樋の架替工事</h5>
								<p class="top_works_ul_add">倉敷市</p>
								<p class="top_works_ul_tag">雨樋の架替工事</p>
							</div>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>
</section>

</main>






<?php get_footer(); ?>