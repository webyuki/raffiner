<!-- クリップパス用SVG -->	
<svg width="0" height="0" style="position: absolute; top: 0; left: 0;">
	<clipPath id="clip01">
		<path d="M338.834,173.804c0,0,13.578-17.657,6.951-51.542C332.949,61.596,286.012,14.659,225.346,1.822
		c-33.885-6.626-51.543,6.953-51.543,6.953s-17.658-13.579-51.542-6.953C61.595,14.659,14.657,61.598,1.821,122.266
		c-6.624,33.882,6.955,51.538,6.955,51.538s-13.579,17.656-6.955,51.538c12.836,60.667,59.774,107.606,120.44,120.443
		c33.884,6.626,51.542-6.953,51.542-6.953s17.658,13.579,51.541,6.954c60.666-12.837,107.604-59.774,120.44-120.441
		C352.412,191.461,338.834,173.804,338.834,173.804z"/>
	</clipPath>
</svg>

<svg class="defs">
	<defs>
		<g id="icon_mail" viewBox="0 0 31.396 24.207">
			<polygon class="st0" points="31.396,3.42 31.396,0 0,0 0,3.42 15.696,13.582 	"/>
			<polygon class="st0" points="15.696,17.695 0,7.533 0,24.207 31.396,24.207 31.396,7.533 	"/>
		</g>
		<g id="icon_tel" viewBox="0 0 31.428 23.387">
			<path class="st0" d="M3.987,23.387c-1.533,0-1.958-0.926-1.958-1.924c0-0.568,0.033-0.674,0.568-1.992
			c0.035-0.141,0.249-0.996,0.319-1.244c0.713-3.065,1.994-4.166,3.311-5.27c1.601-1.351,2.421-2.527,3.025-4.734
			c0.462-1.602,0.462-2.74,0.462-2.848c0-0.285-0.069-1.031-0.427-1.031c-0.143,0-1.173,0.356-1.173,1.281
			c0,0.285,0,0.356,0.178,0.783c0.177,0.498,0.248,0.926,0.248,1.174c0,2.564-5.197,3.418-6.8,3.418C0.212,11,0,10.074,0,8.793
			c0-1.779,0.426-4.164,3.06-5.768C6.904,0.715,12.671,0,15.697,0c2.812,0,8.612,0.678,12.635,3.025
			c2.277,1.354,3.096,3.49,3.096,5.697c0,1.424-0.284,2.277-1.813,2.277c-1.496,0-6.764-0.854-6.764-3.418
			c0-0.32,0.428-1.672,0.428-1.957c0-0.891-1.033-1.281-1.211-1.281c-0.284,0-0.392,0.676-0.392,1.031
			c0,0.25,0.107,4.021,2.457,6.621c0.39,0.426,2.349,2.1,2.668,2.492c1.068,1.351,1.39,2.705,1.96,4.842
			c0.034,0.07,0.354,0.959,0.425,1.102c0.108,0.285,0.18,0.605,0.18,0.996c0,0.926-0.321,1.959-1.96,1.959H3.987z M9.609,14.771
			c0,3.383,2.74,6.09,6.088,6.09c3.346,0,6.086-2.742,6.086-6.09c0-3.416-2.74-6.086-6.086-6.086
			C12.387,8.686,9.609,11.356,9.609,14.771z M14.523,4.379c-0.18,0-0.961-0.143-1.14-0.143c-0.569,0-0.675,0.572-0.675,0.963
			c0,0.818,0.283,0.996,2.989,0.996c2.775,0,2.99-0.178,2.99-1.066c0-0.32-0.108-0.893-0.679-0.893
			c-0.033,0-0.746,0.107-0.817,0.107c-0.498,0.072-0.925,0.106-1.494,0.106C15.127,4.449,14.842,4.416,14.523,4.379z"/>
		</g>
	</defs>
</svg>

<section class="pd-common f_contact relative">
	<div class="">
		<div class="container">
            <div class="text-center">
                <p class="h2 yellowColor mb10">お気軽にご相談ください</p>
            </div>
            <div class="width720 mb30">
                <p class="white lh_xl mb-xs-30 text-center"><!--これはほんの一部のメニューです。本来はお客様のご要望に応じて、最適なコースをご提案いたしますので、-->少しでも興味がございましたらお気軽にお問い合わせください。<br>相談だけでも大歓迎です。</p>
            </div>
            <div class="text-center">
                <div class="footerContactTelCont mb30">
                    <a href="<?php echo home_url(); ?>/contact" class="pt_btn pt_btn_contact h4 mb10"><svg class="icon icon_mail" viewBox="0 0 31.396 24.207"><use xlink:href="#icon_mail"/></svg>お問い合わせはこちら</a>
                    <a href="tel:08019061867" class="pt_btn pt_btn_contact h2"><svg class="icon icon_mail" viewBox="0 0 31.428 23.387"><use xlink:href="#icon_tel"/></svg>080-1906-1867</a>
                </div>
                <div class="text-center mb30">
                    <p class="text_m white">営業時間[10:00〜18:30] 定休日[金]<br>時間外のご予約も承りますので、お気軽にご相談ください。</p>
                </div>
                <a href="https://line.me/R/ti/p/%40myl2462g" target="_blank">
                    <div class="f_banner_line bdBox bgWhiteColor relative">
                        <img class="f_line_icon" src="<?php echo get_template_directory_uri(); ?>/img/line.png" alt="">
                        <p class="h4 mb10">LINEでのやり取りも大歓迎　<span class="f_line_link text_s white tra">友達追加はこちら</span></p>
                        <p class="text_m mb0 pc">お得なクーポン情報や美肌、アンチエイジングに<br>役に立つ情報を発信中です。どうぞご登録ください。</p>
                    </div>
                </a>
			</div>
		</div>
	</div>
</section>

<!-- フッター お問い合わせ ここから -->
<!--
<section class="pd-common f_contact relative">
	<div class="">
		<div class="container">
			<div class="row mb30">
				<div class="col-md-8 col-sm-7">
					<p class="h2 yellowColor mb10">お気軽にご相談ください</p>
					<p class="white lh_xl mb-xs-30">これはほんの一部のメニューです。本来はお客様のご要望に応じて、最適なコースをご提案いたしますので、少しでも興味がございましたらお気軽にお問い合わせください。相談だけでも大歓迎です。</p>
				</div>
				<div class="col-md-4 col-sm-5">
					<div class="flex directionColumn">
						<a href="<?php echo home_url(); ?>/contact" class="pt_btn pt_btn_contact h4 mb10"><svg class="icon icon_mail" viewBox="0 0 31.396 24.207"><use xlink:href="#icon_mail"/></svg>お問い合わせはこちら</a>
						<a href="tel:08019061867" class="pt_btn pt_btn_contact h2"><svg class="icon icon_mail" viewBox="0 0 31.428 23.387"><use xlink:href="#icon_tel"/></svg>080-1906-1867</a>
					</div>
				</div>
			</div>
			<div class="row">
				<div class="col-sm-12">
					<ul class="f_banner ul-2 ul-sm-1">
						<li>
							<div class="f_banner_outer bgWhiteColor relative matchHeight flex justCenter">
								<div class="f_banner_inner flex subColor">初回お申し込みの方　<span class="h2 ygothic bold">限定割引</span></div>
							</div>
						</li>
						<li>
							<div class="f_banner_line bgWhiteColor relative matchHeight">
								<img class="f_line_icon" src="<?php echo get_template_directory_uri(); ?>/img/line.png" alt="">
								<p class="h4 mb10">LINEでのやり取りも大歓迎　<a href="https://line.me/R/ti/p/%40myl2462g" target="_blank" class="f_line_link text_s white">友達追加はこちら</a></p>
								<p class="text_m mb0">お得なクーポン情報や美肌、アンチエイジングに<br>役に立つ情報を発信中です。どうぞご登録ください。</p>
							</div>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>
</section>
-->
<!-- フッター お問い合わせ ここまで -->

<section class="f_info mt50 mb-xs-50">
	<div class="container">
		<div class="row">
			<div class="col-sm-12 text-center text-center-xs fs09">
				<p class="mb20">毛穴エクストラクション&amp;アンチエイジングサロン ラ・フィーネ</p>
				<a href="<?php echo home_url(); ?>"><img class="f_logo mb20" src="<?php echo get_template_directory_uri(); ?>/img/logo.png" alt="岡山のフェイシャル専門エステサロン ラ・フィーネ"></a>
				<p>岡山市北区下中野708-105 ブルージュ下中野102</p>
				<p class="mb50">TEL:080-1906-1867</p>
				<p>copyright© 2019 raffiner all rights reserved.</p>
			</div>
		</div>
	</div>
</section>

<script>
	$('.top_pro_slider').slick({
	  slidesToShow: 4,
	  centerMode: true,
	  arrows: false,
	  autoplay: true,
	  autoplaySpeed: 0, //待ち時間を０に
	  speed: 8000, // スピードをゆっくり
	  swipe: false, // 操作による切り替えはさせない
	  cssEase: 'linear', // 切り替えイージングを'linear'に
	  // 以下、操作後に止まってしまう仕様の対策
	  pauseOnFocus: false,
	  pauseOnHover: false,
	  pauseOnDotsHover: false,

	  // 以下、レスポンシブ
	  responsive: [
		{
		  breakpoint: 767,
		  settings: {
			slidesToShow: 2,
		  }
		}
	  ]
	});
</script>

<?php wp_footer(); ?>
<!--フォントプラス-->
<script type="text/javascript" src="//webfont.fontplus.jp/accessor/script/fontplus.js?tUOPpkwcnxA%3D&box=DPC0QA6Ps9Q%3D&aa=1&ab=2" charset="utf-8"></script>

<script src="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.js"></script>



<script>
AOS.init({
	placement	: "bottom-top",
	duration: 1000,
});
</script>
</body>
</html>
 