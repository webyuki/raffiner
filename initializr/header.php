<!doctype html>
<html>
<head>
<meta charset="utf-8">
<!--キャッシュクリア-->
<meta http-equiv="Pragma" content="no-cache">
<meta http-equiv="Cache-Control" content="no-cache">
<meta http-equiv="Expires" content="0">
<!--キャッシュクリア終了-->
<meta http-equiv="X-UA-Compatible" content="IE=edge,chrome=1">
<meta name="viewport" content="width=device-width, initial-scale=1">
<title><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></title>
<?php wp_head(); ?>
<link rel="icon" type="image/x-icon" href="<?php echo get_template_directory_uri();?>/favicon.ico">
<link rel="apple-touch-icon" href="<?php echo get_template_directory_uri();?>/apple-touch-icon.png"><link href="https://cdn.rawgit.com/michalsnik/aos/2.1.1/dist/aos.css" rel="stylesheet">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/modal.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/loaders.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap.min.css">
<link href="https://maxcdn.bootstrapcdn.com/font-awesome/4.7.0/css/font-awesome.min.css" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=EB+Garamond" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Roboto" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Bebas+Neue&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/earlyaccess/hannari.css" rel="stylesheet">

<link href="https://fonts.googleapis.com/css?family=Roboto+Condensed:400,700&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Parisienne&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Comfortaa&display=swap" rel="stylesheet">
<link href="https://fonts.googleapis.com/css?family=Lato&display=swap" rel="stylesheet">
<link rel="stylesheet" type="text/css" href="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.css"/>
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/bootstrap-theme.min.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/main.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/style.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/slick-theme.css">
	<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/lightbox.css">
<link rel="stylesheet" href="<?php echo get_template_directory_uri();?>/css/style.css">

<script src="<?php echo get_template_directory_uri();?>/js/vendor/modernizr-2.8.3-respond-1.4.2.min.js"></script>
<script src="https://ajax.googleapis.com/ajax/libs/jquery/1.11.2/jquery.min.js"></script>
<script>window.jQuery || document.write('<script src="<?php echo get_template_directory_uri();?>/js/vendor/jquery-1.11.2.min.js"><\/script>')</script>
<script src="<?php echo get_template_directory_uri();?>/js/vendor/bootstrap.min.js"></script>
<script type="text/javascript" src="//cdn.jsdelivr.net/npm/slick-carousel@1.8.1/slick/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/jquery.matchHeight-min.js"></script>
<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/parallax/parallax.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/main.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/slick.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/lightbox.js" type="text/javascript"></script>
	<script src="https://cdnjs.cloudflare.com/ajax/libs/gsap/2.0.1/TweenMax.min.js"></script>
<script src="<?php echo get_template_directory_uri();?>/js/wavify/wavify.js" type="text/javascript"></script>
<script src="<?php echo get_template_directory_uri();?>/js/wavify/jquery.wavify.js" type="text/javascript"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/parallax-background.min.js"></script>
	<script src="<?php echo get_template_directory_uri();?>/js/jquery.particleground.min.js"></script>
<script>
/*高さ合わせる*/
$(function(){
	$('.matchHeight').matchHeight();
});

</script>
<script type="text/javascript">
$(function() {
	$('.slick-box').slick({
		prevArrow: '<div class="slider-arrow slider-prev fa fa-angle-left"></div>',
		nextArrow: '<div class="slider-arrow slider-next fa fa-angle-right"></div>',
		autoplay: true,
		autoplaySpeed: 4000,
		fade: true,
		speed: 2000,
	});
});
</script>


<!-- Google Tag Manager -->
<script>(function(w,d,s,l,i){w[l]=w[l]||[];w[l].push({'gtm.start':
new Date().getTime(),event:'gtm.js'});var f=d.getElementsByTagName(s)[0],
j=d.createElement(s),dl=l!='dataLayer'?'&l='+l:'';j.async=true;j.src=
'https://www.googletagmanager.com/gtm.js?id='+i+dl;f.parentNode.insertBefore(j,f);
})(window,document,'script','dataLayer','GTM-TRQX2CJ');</script>
<!-- End Google Tag Manager -->

</head>

<body <?php body_class();?>>
<!--[if lt IE 8]>
<p class="browserupgrade">You are using an <strong>outdated</strong> browser. Please <a href="http://browsehappy.com/">upgrade your browser</a> to improve your experience.</p>
<![endif]-->


<!-- Google Tag Manager (noscript) -->
<noscript><iframe src="https://www.googletagmanager.com/ns.html?id=GTM-TRQX2CJ"
height="0" width="0" style="display:none;visibility:hidden"></iframe></noscript>
<!-- End Google Tag Manager (noscript) -->

<!--ローディングCSS-->

<div id="js-loader">
	<div class="loader-inner ball-pulse">
	  <div></div>
	  <div></div>
	  <div></div>
	</div>
</div>
<style>
#js-loader {
	display: flex;
    position: fixed;
    height: 100vh;
    width: 100vw;
    z-index: 9999;
    background: #bf9f72;
    align-items: center;
    justify-content: center;
}

.ball-pulse-sync>div, .ball-pulse>div, .ball-scale-random>div, .ball-scale>div {
    background-color: #fff;
}
</style>

<script>
// ローディング画面をフェードインさせてページ遷移
$(function(){
    // リンクをクリックしたときの処理。外部リンクやページ内移動のスクロールリンクなどではフェードアウトさせたくないので少し条件を加えてる。
    $('a[href ^= "https://newstella.co.jp"]' + 'a[target != "_blank"]').click(function(){
        var url = $(this).attr('href'); // クリックされたリンクのURLを取得
        $('#js-loader').fadeIn(600);    // ローディング画面をフェードイン
        setTimeout(function(){ location.href = url; }, 800); // URLにリンクする
        return false;
    });
});

// ページのロードが終わった後の処理
$(window).load(function(){
  $('#js-loader').delay(300).fadeOut(400); //ローディング画面をフェードアウトさせることでメインコンテンツを表示
});
 
// ページのロードが終わらなくても10秒たったら強制的に処理を実行
$(function(){ setTimeout('stopload()', 10000); });
function stopload(){
  $('#js-loader').delay(300).fadeOut(400); //ローディング画面をフェードアウトさせることでメインコンテンツを表示
}

</script>





<header>
	<div class="container">
		<div class="headerTop flex alignStart justBetween">
			<div class="headerLeft">
				<div class="headerText absolute"><h2 class="text_s mt10 mt-xs-0">毛穴エクストラクション&amp;アンチエイジングサロン<br>ラ・フィーネ</h2></div>
				<h1 class="logo remove mt70 mt-sm-70 mt-xs-30 mb-xs-10"><a href="<?php echo home_url();?>"><?php wp_title('',true); ?><?php if(wp_title('',false)) { ?> | <?php } ?><?php bloginfo('name'); ?></a></h1>
			</div>
			<div class="headerRight">
				<div class="header_contact text-right mt10">
				    <a href="https://www.instagram.com/miyuki_raffiner/" target="_blank" class="h_tel_insta">
				        <img src="<?php echo get_template_directory_uri();?>/img/top_news_banner_ico_insta.png" alt="">
				    </a>
					<a href="tel:08019061867" class="h_tel_link">
						<p class="text_s">営業時間[10:00〜18:30] 定休日[金]</p>
						<img src="<?php echo get_template_directory_uri(); ?>/img/h_tel.png" alt="">
					</a>
					<a href="<?php echo home_url();?>/contact" class="pt_btn pt_btn_h_contact bold"><svg class="icon icon_mail" viewBox="0 0 31.396 24.207"><use xlink:href="#icon_mail"/></svg>お問い合わせ</a>
				</div>
				<div class="headerMenu pc text-center bold relative">
					<?php wp_nav_menu( array( 'menu_class' => 'flex justEnd alignCenter titleJp' ) ); ?>
				</div>	
			</div>
		</div>
	</div>
</header>

<!-- 開閉用ボタン -->

<div class="menu-btn sp" id="js__btn">
    <!--<span data-txt-menu="MENU" data-txt-close="CLOSE"></span>-->
	<div class="menu-trigger" href="#">
		<span></span>
		<span></span>
		<span></span>
		<span class="headerHunbergerText">MENU</span>
	</div>	
</div>

<!-- モーダルメニュー -->
<nav class="menu sp" id="js__nav">
	<?php wp_nav_menu( array( 'menu_class' => '' ) ); ?>
</nav>

<section class="contactSide absolute pc">
	<a href="<?php echo home_url();?>/contact" class="contactSideWap text-center">
		<div class="engTitle mainColor h5 titleBd">mail</div>
		<div class="titleJp mColor text_s grayColor">メール<span class="">は<br>こちらから</span></div>
	</a>
	<a href="https://line.me/R/ti/p/%40myl2462g" target="_blank" class="contactSideWap line text-center">
		<div class="text_m bold titleBd">LINE</div>
		<div class="titleJp mColor text_s grayColor">友達登録<span class="">は<br>こちらから</span></div>
	</a>
	
</section>



<section class="footerTelSp sp">
	<ul class="flex">
		<li class="footerTelSpFlexLi text-center white">
			<a href="tel:08019061867"><i class="fa fa-phone" aria-hidden="true"></i><div class="text_s bold">お電話</div></a>
		</li>
		<li class="footerTelSpFlexLi text-center white">
			<a href="<?php echo home_url();?>/contact"><i class="fa fa-envelope-o" aria-hidden="true"></i><div class="text_s bold">お問い合わせ</div></a>
		</li>
		<li class="footerTelSpFlexLi text-center white">
			<a href="https://line.me/R/ti/p/%40myl2462g" target="_blank"><i class="fa fa-comment-o" aria-hidden="true"></i><div class="text_s bold">LINE</div></a>
		</li>
	</ul>
</section>



<script>
$(function () {
    var $body = $('body');

    //開閉用ボタンをクリックでクラスの切替え
    $('#js__btn').on('click', function () {
        $body.toggleClass('open');
    });

    //メニュー名以外の部分をクリックで閉じる
    $('#js__nav').on('click', function () {
        $body.removeClass('open');
    });
});

</script>

<script>
$(function() {
	$(window).on('load resize', function() {
		$('.parallax').parallaxBackground();
	});
});
</script>