<?php get_header(); ?>
<main>
	
<section class="under_fv bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/under_howto_fv.png'); background-color: #ebebde;">
	<h2 class="under_fv_title">ご購入について</h2>	
</section>
	
<section class="pd-common" style="background-color: #ebebde;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="ul-3 ul-xs-1 top_pro_link_ul" data-aos="fade-up">
					<li>
						<a href="#howto_net" class="clearfix">
							<div class="top_pro_link_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_howto02.jpg');"></div>
							<p class="top_pro_link_txt">ネットでご注文<br>したい方へ</p>
						</a>
					</li>
					<li>
						<a href="#howto_shop" class="clearfix">
							<div class="top_pro_link_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_howto03.jpg');"></div>
							<p class="top_pro_link_txt">取り扱い店舗で<br>ご購入したい方へ</p>
						</a>
					</li>
					<li>
						<a href="#howto_direct" class="clearfix">
							<div class="top_pro_link_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/top_howto01.jpg');"></div>
							<p class="top_pro_link_txt">取扱いご希望の<br>業者様へ</p>
						</a>
					</li>
				</ul>
			</div>
		</div>
	</div>	
</section>

<section class="pd-common" id="howto_net" style="background-color: #ebebde;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="order_net_area" data-aos="fade-up">
					<div class="text-center text-center-xs"><h3 class="pt_title02"><span>ネット</span>でご注文したい方へ</h3></div>
					<div class="row mb20">
						<div class="col-sm-8">
							<p class="mb30">倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
							<p class="mb30">倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
							<p class="mb30">倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
						</div>
						<div class="col-sm-4">
							<img class="order_net_icon" src="<?php echo get_template_directory_uri(); ?>/img/order_net.png" alt="ネットでご注文したい方へ">
						</div>
					</div>
					
					<ul class="ul-3 ul-xs-1 order_net_ul mb30">
						<li>
							<div class="order_net_ul_img_outer">
								<div class="order_net_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/order_net01.jpg');"></div>
							</div>
							<p class="order_net_ul_title">全国発送対応可能</p>
							<p>倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
						</li>
						<li>
							<div class="order_net_ul_img_outer">
								<div class="order_net_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/order_net02.jpg');"></div>
							</div>
							<p class="order_net_ul_title">クレジット決済対応</p>
							<p>倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
						</li>
						<li>
							<div class="order_net_ul_img_outer">
								<div class="order_net_ul_img bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/order_net03.jpg');"></div>
							</div>
							<p class="order_net_ul_title">時間指定もできます</p>
							<p>倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
						</li>
					</ul>
					
					<div class="text-center text-center-xs"><a href="<?php echo home_url(); ?>/menu" class="pt_btn01">取り扱い商品はこちら</a></div>
				</div>
			</div>
		</div>
	</div>	
</section>

<section class="pd-common" id="howto_shop" style="background-color: #ebebde;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="order_net_area" data-aos="fade-up">
					<div class="text-center text-center-xs"><h3 class="pt_title02"><span>取扱い店舗</span><br class="visible-xs">でご購入したい方へ</h3></div>
					<h4 class="order_shop_subtitle text-center text-center-xs mb30">取扱い店舗のご紹介</h4>
					<p class="mb30 text-center">倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで<br>安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
					<ul class="order_buy_ul hidden-xs">
						<li>
							<p class="bold">店舗名</p>
						</li>
						<li>
							<p class="bold">住所</p>
						</li>
						<li>
							<p class="bold">電話番号</p>
						</li>
					</ul>
					<ul class="order_buy_ul">
						<li>
							<p>道の駅 かよう</p>
						</li>
						<li>
							<p>〒716-1551<br>岡山県加賀郡吉備中央町北1974</p>
						</li>
						<li>
							<p>0866-55-6008</p>
						</li>
					</ul>
					<ul class="order_buy_ul">
						<li>
							<p>道の駅 かよう</p>
						</li>
						<li>
							<p>〒716-1551<br>岡山県加賀郡吉備中央町北1974</p>
						</li>
						<li>
							<p>0866-55-6008</p>
						</li>
					</ul>
					<ul class="order_buy_ul">
						<li>
							<p>道の駅 かよう</p>
						</li>
						<li>
							<p>〒716-1551<br>岡山県加賀郡吉備中央町北1974</p>
						</li>
						<li>
							<p>0866-55-6008</p>
						</li>
					</ul>
					<ul class="order_buy_ul">
						<li>
							<p>道の駅 かよう</p>
						</li>
						<li>
							<p>〒716-1551<br>岡山県加賀郡吉備中央町北1974</p>
						</li>
						<li>
							<p>0866-55-6008</p>
						</li>
					</ul>
					<ul class="order_buy_ul mb30">
						<li>
							<p>道の駅 かよう</p>
						</li>
						<li>
							<p>〒716-1551<br>岡山県加賀郡吉備中央町北1974</p>
						</li>
						<li>
							<p>0866-55-6008</p>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common mb20" id="howto_direct" style="background-color: #ebebde;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="order_net_area" data-aos="fade-up">
					<div class="text-center text-center-xs"><h3 class="pt_title02"><span>取扱いご希望の</span>業者様へ</h3></div>
					<div class="row mb50">
						<div class="col-sm-8">
							<p class="mb30">倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
							<p class="mb30">倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
							<p class="mb30">倉ファームは自然豊かな環境の中、水にこだわり 昔ながらのスタイルで安心・安全・おいしい野菜を、採れたその日のうちに出荷する農園です。</p>
						</div>
						<div class="col-sm-4">
							<img class="order_net_icon" src="<?php echo get_template_directory_uri(); ?>/img/order_customer.png" alt="取扱いご希望の業者様へ">
						</div>
					</div>
					
					<ul class="ul-2 ul-xs-1 text-center text-center-xs f_contact_ul">
						<li>
							<a href="<?php echo home_url(); ?>/contact" class="pt_btn01">お問い合わせフォーム</a>
						</li>
						<li>
							<a href="tel:0869449488" class="f_contact_btn"><span>    (086)-944-9488</span><br>09:00〜17:00</a>
						</li>
					</ul>
				</div>
			</div>
		</div>
	</div>	
</section>

</main>






<?php get_footer(); ?>