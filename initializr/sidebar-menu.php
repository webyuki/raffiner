<div id="sidebar">



<?php 
	//if($slug!="voice"):	//カテゴリー記事を見ている場合
?>
	<!--同じカテゴリーの記事を出力-->
	<?php get_template_part('parts/sidebar-menu-article'); ?>
		
	<!--同じカテゴリーの施工事例を出力-->	
	<?php get_template_part('parts/sidebar-example'); ?>
	
<?php /*elseif(is_singular('example') || is_post_type_archive('example')  ):*/?>	

	<!--同じカテゴリーの記事を出力-->
	<?php //get_template_part('parts/sidebar-article'); ?>
		
	<!--同じカテゴリーの施工事例を出力-->	
	<?php //get_template_part('parts/sidebar-example'); ?>





<?php //endif; ?>	
	
	
	    
	
	<aside>
		<h2 class="h3 title_main  bold title_margin">施工事例カテゴリー</h2>
		<?php
			// カスタム分類名
			$taxonomy = 'cat_example';
			// パラメータ 
			$args = array(
				// 子タームの投稿数を親タームに含める
				'pad_counts' => true,  
				// 投稿記事がないタームも取得
				'hide_empty' => false
			);
			// カスタム分類のタームのリストを取得
			$terms = get_terms( $taxonomy , $args );
			if ( count( $terms ) != 0 ) {
				echo '<ul>';
				// タームのリスト $terms を $term に格納してループ
				foreach ( $terms as $term ) {
					// タームのURLを取得
					$term = sanitize_term( $term, $taxonomy );
					$term_link = get_term_link( $term, $taxonomy );
					if ( is_wp_error( $term_link ) ) {
						continue;
					}
					// 子タームの場合はCSSクラス付与
					if( $term->parent != 0 ) {
						echo '<li class="children">';
					} else {
						echo '<li>';
					}
					// タームのURLと名称を出力
					echo '<a href="' . esc_url( $term_link ) . '">> ' . $term->name . '</a>(' . $term->count . ')';
					echo '</li>';
				}
			echo '</ul>';
			}
		?>	
	</aside>


	<aside>
		<a href="<?php echo home_url();?>">
			<img class="shadow" src="<?php echo get_template_directory_uri();?>/img/bn_img_concept.jpg"/>
		</a>
	</aside>
	<aside>
		<a href="<?php echo home_url();?>/voice">
			<img class="shadow" src="<?php echo get_template_directory_uri();?>/img/bn_img_voice.jpg"/>
		</a>
	</aside>
	<aside>
		<a href="<?php echo home_url();?>">
			<img class="shadow" src="<?php echo get_template_directory_uri();?>/img/bn_ico_success.jpg"/>
		</a>
	</aside>
	<aside>
		<a href="<?php echo home_url();?>/staff">
			<img class="shadow" src="<?php echo get_template_directory_uri();?>/img/bn_img_staff.jpg"/>
		</a>
	</aside>
</div>