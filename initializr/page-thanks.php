<?php get_header(); ?>

<main>
	
<section class="under_fv parallax relative"
	 data-parallax-bg-image="<?php echo get_template_directory_uri(); ?>/img/top_about_bg.jpg"
	 data-parallax-bg-position="center bottom"
	 data-parallax-speed="0.4"
	 data-parallax-direction="down">
	<div class="under_fv_txt absolute">
		<h2 class="h2 mainColor">送信完了</h2>
		<p class="engTitle h1 subColor">thanks</p>
	</div>
</section>



<section class="pd-common relative paperBgUnder">
	<div class="container">
		<div class="">
			<div class="contInCont" data-aos="fade-up">
				<div class="mb30 text-center width780">
					<p>お問い合わせの送信が完了しました。</p>
					<p>担当者から折り返させてご連絡させて頂きますので、今しばらくお待ち下さい。</p>
				</div>
				<a class="telLink fontEn h0 text-center bold block mb0" href="tel:08019061867">080-1906-1867</a>
                <p class="text_m gray text-center mb50">営業時間[10:00〜18:30] 定休日[金]</p>
				<div class="contactForm" data-aos="fade-up"><?php echo do_shortcode('[mwform_formkey key="29"]'); ?></div>
			</div>
		</div>
	</div>
</section>


<?php 
	while ( have_posts() ) : the_post();
?>
<?php the_content();?>
<?php //get_template_part('content'); ?>
<?php 
	endwhile;
?>	



</main>






<?php get_footer(); ?>