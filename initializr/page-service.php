<?php get_header(); ?>

<main>
	
<section class="pd-common">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<p class="h1 engTitle bold mt40 mt-xs-20">SERVICE</p>
				<h2 class="h2 bold">サービス内容</h2>
			</div>
		</div>
	</div>	
</section>

<section class="pd-common pb0" id="service01" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="service_area top_recruit_area pt_grad_after relative mb50">
					<div class="text-center-xs"><p class="engMainTitle engTitle white">01 <br class="visible-xs"><span class="h1">PA Equipment Brought Plan</span></p></div>
					<div class="text-center-xs"><h3 class="h3 bold mb50 mb-xs-20">音響機材持込プラン</h3></div>
					<p class="mb80 mb-xs-30">音響機材一式、または一部を現場へ持込み、設営・調整し、本番運用するプランです。<br>弊社のメイン業務であり、お客様のご要望には細やかに対応させていただきます。<br>様々な催事のコンセプトに最適なプランニングをし、主催者様の音のイメージをカタチにします。</p>
				</div>
			</div>
			<div class="col-sm-12">
				<ul class="service01_ul ul-3 ul-sm-2 ul-xs-1">
					<li>
						<div class="service_ul_img bg-common mb10" style="background-image: url('<?php echo get_template_directory_uri();?>/img/service01_01.jpg');"></div>
						<h4 class="h4 bold text-center mb10">学園祭でのバンドステージ</h4>
						<p>バンド対応可能な音響機材はもちろん、当日演奏に必要な楽器レンタルのご相談も引き受けます。</p>
					</li>
					<li>
						<div class="service_ul_img bg-common mb10" style="background-image: url('<?php echo get_template_directory_uri();?>/img/service01_02.jpg');"></div>
						<h4 class="h4 bold text-center mb10">多目的野外広場でのコンサート</h4>
						<p>音響機材一式持込みのケースです。会場の広さ、観客動員数、音楽ジャンルなどによって使用する機材をセレクトし、現場に最適なセッティングで臨みます。</p>
					</li>
					<li>
						<div class="service_ul_img bg-common mb10" style="background-image: url('<?php echo get_template_directory_uri();?>/img/service01_03.jpg');"></div>
						<h4 class="h4 bold text-center mb10">各種ダンス教室などの発表会</h4>
						<p>市民会館等の各種ホールで開催される発表会など、会場機材へ弊社持込み機材（ミキサーや再生機など）を繋ぎ込み、オペレートする場合もあります。</p>
						<p class="text_m lightgrayColor">※ホール設備費等は別途かかる場合があります。</p>
					</li>
					<li>
						<div class="service_ul_img bg-common mb10" style="background-image: url('<?php echo get_template_directory_uri();?>/img/service01_04.jpg');"></div>
						<h4 class="h4 bold text-center mb10">ショッピングモール内<br>イベントスペースでの各種イベント</h4>
						<p>バンド対応可能な音響機材はもちろん、当日演奏に必要な楽器レンタルのご相談も引き受けます。</p>
					</li>
					<li>
						<div class="service_ul_img bg-common mb10" style="background-image: url('<?php echo get_template_directory_uri();?>/img/service01_05.jpg');"></div>
						<h4 class="h4 bold text-center mb10">体育館での行事や式典、<br>講演会、演説会</h4>
						<p>建築的に残響が多めでスピーチが聴き取りにくい空間ですが、最適な機材のセレクト、配置、調整で驚くほど聴きやすくなります。</p>
					</li>
					<li>
						<div class="service_ul_img bg-common mb10" style="background-image: url('<?php echo get_template_directory_uri();?>/img/service01_06.jpg');"></div>
						<h4 class="h4 bold text-center mb10">ホテルの宴会場でのパーティー、<br>ディナーショー</h4>
						<p>小規模〜大規模な催事までコンパクトかつパワフルな機材を設置し、客席からの視線を妨げない会場作りができます。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common pb0" id="service02" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="service_area top_recruit_area pt_grad_after relative mb50">
					<div class="text-center-xs"><p class="engMainTitle engTitle white">02 <br class="visible-xs"><span class="h1">PA Staff Only Plan</span></p></div>
					<div class="text-center-xs"><h3 class="h3 bold mb50 mb-xs-20">スタッフ派遣プラン</h3></div>
					<p class="mb20">ホールやライブハウス、コンベンションなど会場設備の整った施設にて、オペレーターのみ現場にお伺いするプランです。<br>何点かの環境さえ整えば、持ち込み機材なしでも現場運用することが可能です。</p>
					<p class="text_m lightgrayColor">※催事の内容により、持込み機材や、追加のスタッフが発生する場合があります。</p>
				</div>
			</div>
			<div class="col-sm-12">
				<h4 class="h3 bold text-center mb20">主な業務事例</h4>
				<p class="text-center mb30">下記実績以外にも様々な業務経験がありますので、お気軽にお問い合わせください。</p>
				<ul class="service01_ul ul-3 ul-xs-1">
					<li>
						<p class="service02_txtarea pt_grad matchHeight">ホールやコンベンションでの式典や講演会、ライブハウスやカフェでのイベントなど、会場機材だけで対応可能な場合。</p>
					</li>
					<li>
						<p class="service02_txtarea pt_grad matchHeight">音響機材を持っているイベント会社様で、専門的な音響のオペレーターが必要な場合。</p>
					</li>
					<li>
						<p class="service02_txtarea pt_grad matchHeight">他社の音響会社様からのヘルプ要請への対応。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common pb0" id="service03" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="service_area top_recruit_area pt_grad_after relative mb50">
					<div class="text-center-xs"><p class="engMainTitle engTitle white">03 <br class="visible-xs"><span class="h1">Events Total Plan</span></p></div>
					<div class="text-center-xs"><h3 class="h3 bold mb50 mb-xs-20">イベントトータルプラン<h3></div>
					<p class="mb80 mb-xs-30">音響のみならず、舞台・照明・映像・特効・イベント備品など、お客様のニーズに合わせてトータルサポートさせて頂くプランです。<br>催事を行う際に、音響だけではなく舞台や会場設営に関するご相談もまとめてお伺いいたします。<br>イベントコンセプトをトータル的にカタチにさせて頂きますので、主催者様は技術面での心配をすることなく、より良い企画・運営に集中することができます。</p>
				</div>
			</div>
			<div class="col-sm-12">
				<h4 class="h3 bold text-center mb20">主な業務事例</h4>
				<p class="text-center mb30">下記実績以外にも様々な業務経験がありますので、お気軽にお問い合わせください。</p>
				<ul class="service01_ul ul-3 ul-sm-2 ul-xs-1 text-center">
					<li>
						<div class="service_ul_img bg-common mb10" style="background-image: url('<?php echo get_template_directory_uri();?>/img/service03_01.jpg');"></div>
						<h5 class="h4 bold text-center mb10">野外特設会場での式典、パーティー</h5>
						<p class="text-left">駐車場や広場など、普段何もないスペースに特設会場を設営できます。ステージ周り（舞台、照明、映像、音響など）で演出効果を高め、テント、イベントグッズなどご要望に応じて必要なものを手配させて頂きます。</p>
					</li>
					<li>
						<div class="service_ul_img bg-common mb10" style="background-image: url('<?php echo get_template_directory_uri();?>/img/service03_02.jpg');"></div>
						<h5 class="h4 bold text-center mb10">映像技術の伴う催事など</h5>
						<p class="text-left">ライブや各種イベント、ホテルでの式典、ホールでの講演会のライブカメラなど、映像機材の必要なシーンが最近特に増えてきています。【Jointly】では、豊富なネットワークで、大型スクリーンやLEDビジョンといった映像技術の御相談も引き受けることができます。</p>
					</li>
				</ul>
			</div>
		</div>
	</div>	
</section>
	
<section class="pd-common" id="service04" data-aos="fade-up">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<div class="service_area top_recruit_area pt_grad_after relative mb100">
					<div class="text-center-xs"><p class="engMainTitle engTitle white">04 <br class="visible-xs"><span class="h1">Rental Plan</span></p></div>
					<div class="text-center-xs"><h3 class="h3 bold mb50 mb-xs-20">機材貸出プラン</h3></div>
					<p class="mb80 mb-xs-30">音響機材一式、または一部を現場へ持込み、設営・調整し、本番運用するプランです。<br>弊社のメイン業務であり、お客様のご要望には細やかに対応させていただきます。<br>様々な催事のコンセプトに最適なプランニングをし、主催者様の音のイメージをカタチにします。</p>
				</div>
			</div>
			<div class="col-sm-12">
				<div class="text-center"><p class="engMainTitle engTitle white">EQUIPMENT</p></div>
				<h4 class="h3 bold white mb30 mb-xs-30 text-center">機材リスト</h4>
				<table class="service_equipment_table">
					<tbody>
						<tr>
							<th>品名</th>
							<th>メーカー</th>
							<th>型番</th>
							<th>数量</th>
						</tr>
						<tr>
							<td>SP</td>
							<td>JBL</td>
							<td>VRX932LA-1</td>
							<td>4</td>
						</tr>
						<tr>
							<td>SP</td>
							<td>JBL</td>
							<td>VRX918S</td>
							<td>2</td>
						</tr>
						<tr>
							<td>SP</td>
							<td>EV</td>
							<td>SX300</td>
							<td>4</td>
						</tr>
						<tr>
							<td>SP Stand</td>
							<td>K＆M 他</td>
							<td>各種</td>
							<td>―</td>
						</tr>
						<tr>
							<td>Amp</td>
							<td>AMCRON</td>
							<td>XTi6002</td>
							<td>2</td>
						</tr>
						<tr>
							<td>Amp</td>
							<td>AMCRON</td>
							<td>XTi2002</td>
							<td>2</td>
						</tr>
						<tr>
							<td>GEQ</td>
							<td>YAMAHA</td>
							<td>Q2031B</td>
							<td>1</td>
						</tr>
						<tr>
							<td>Mix</td>
							<td>MIDAS</td>
							<td>M32</td>
							<td>1</td>
						</tr>
						<tr>
							<td>Mix</td>
							<td>YAMAHA</td>
							<td>MG16XU</td>
							<td>1</td>
						</tr>
						<tr>
							<td>UPS</td>
							<td>APC</td>
							<td>SMT750J</td>
							<td>1</td>
						</tr>
						<tr>
							<td>Play/Rec</td>
							<td>TASCAM</td>
							<td>MDCD1mkⅢ</td>
							<td>1</td>
						</tr>
						<tr>
							<td>Play/Rec</td>
							<td>TASCAM</td>
							<td>SS-R100</td>
							<td>1</td>
						</tr>
						<tr>
							<td>Play/Rec</td>
							<td>Roland</td>
							<td>SP-404SX</td>
							<td>1</td>
						</tr>
						<tr>
							<td>Mic/DI</td>
							<td>Shure 他</td>
							<td>SM58 etc 各種</td>
							<td>―</td>
						</tr>
						<tr>
							<td>Mic Stand</td>
							<td>K＆M 他</td>
							<td>ST210 etc 各種</td>
							<td>―</td>
						</tr>
						<tr>
							<td>PA Set</td>
							<td>YAMAHA</td>
							<td>StagePass600i</td>
							<td>1</td>
						</tr>
						<tr>
							<td>Multi Cable</td>
							<td>CANARE他</td>
							<td>16ch etc 各種</td>
							<td>―</td>
						</tr>
					</tbody>
				</table>
			</div>
		</div>
	</div>	
</section>
	
</main>

<?php get_footer(); ?>