<?php get_header(); ?>

<main style="background-color: #eae7e5;">
	
<section class="under_fv bg-common" style="background-image: url('<?php echo get_template_directory_uri(); ?>/img/detail_fv.jpg');">
	<div class="under_fv_titlearea">
		<p class="pt_title_eng text-center text-center-xs mb0">WORKS</p>
		<h2 class="pt_title_jp text-center text-center-xs mb0"><?php single_term_title( ); ?></h2>	
	</div>
</section>

<section class="pd-common pt0" id="top_works" style="background-color: #ebe8e6;">
	<div class="container">
		<div class="row">
			<div class="col-sm-12">
				<ul class="list_link">
                
                    <li><a href="<?php echo home_url();?>/works">全て</a></li>
                    <?php $categories = get_categories(array('taxonomy' => 'works_cate')); if ( $categories ) : ?>
                        <?php foreach ( $categories as $category ): ?>
                            <li><a href="<?php echo home_url();?>/works_cate/<?php echo esc_html( $category->slug);?>"><?php echo wp_specialchars( $category->name ); ?></a></li>
                        <?php endforeach; ?>
                    <?php endif; ?>
                
				</ul>
				<ul class="top_works_ul ul-2 ul-xs-1 mb50">
                    <?php			
                        while ( have_posts() ) : the_post();
                            get_template_part('content-post-works-archive'); 
                        endwhile;
                    ?>
				</ul>
			</div>
		</div>
		<?php get_template_part( 'parts/pagenation' ); ?>
	</div>
</section>

</main>



<?php get_footer(); ?>