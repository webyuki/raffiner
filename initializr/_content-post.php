<?php
	$category = get_the_category();
	$cat_id   = $category[0]->cat_ID;
	$cat_name = $category[0]->cat_name;
	$cat_slug = $category[0]->category_nicename;
?>

<!-- カテゴリーIDを表示したい所に -->
<?php //echo $cat_id; ?>

<!-- カテゴリー名を表示したい所に -->
<?php //echo $cat_name; ?>

<!-- カテゴリースラッグを表示したい所に -->
<?php //echo $cat_slug; ?>



					<div class="row mb30">
						<div class="col-sm-6">
							<a href="<?php the_permalink();?>" class="">
							<?php if (has_post_thumbnail()): ?>
<?php 
	// アイキャッチ画像のIDを取得
	$thumbnail_id = get_post_thumbnail_id(); 
	// mediumサイズの画像内容を取得（引数にmediumをセット）
	$eye_img = wp_get_attachment_image_src( $thumbnail_id , 'full' );
	$eye_img_s = wp_get_attachment_image_src( $thumbnail_id , 'thumb_size_s_false',false );
?>
									<div class="topArt__img bgup " style="background-image:url(<?php echo $eye_img_s[0];?>);"></div>
    <?php else: ?>
									<div class="topArt__img bgup " style="background-image:url(<?php echo get_template_directory_uri();?>/img/thumb_sample.png);"></div>
    <?php endif; ?>								
							</a>
						</div>
						<div class="col-sm-6">
							<div class="topArt__art">
								<p>
									<span class="cate white mb10 text_s <?php echo $cat_slug; ?>"><a class="white" href="<?php echo home_url();?>/category/<?php echo $cat_slug; ?>"><span class="glyphicon glyphicon-folder-open"></span><?php echo $cat_name; ?></a></span>
									<span class="day">
										<span class="glyphicon glyphicon-calendar"></span><span class="sweetpineapple"><?php the_time('Y/m/d'); ?></span>　　
										<?php if (get_the_modified_date('Y/n/j') != get_the_time('Y/n/j')) : ?>
										<span class="glyphicon glyphicon-repeat"></span><span class="sweetpineapple"><?php the_modified_date('Y/m/d') ?></span>
										<?php endif; ?>
									</span>

								</p>
								<a href="<?php the_permalink();?>" class="">
									<h4 class="artTitle bold mb10 opa"><?php the_title();?></h4>
								</a>
								<p class="text_sm descri"><?php echo get_the_excerpt();?></p>
							</div>
						</div>
					</div>




